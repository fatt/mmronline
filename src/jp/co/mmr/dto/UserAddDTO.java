package jp.co.mmr.dto;

public class UserAddDTO {
	 //アクセサメソッド用変数定義
    private int id;
    private String userName;
    private String userHurigana;
    private String userPassword;
    private String checkPassword;
    private String mailAddress;
    private String checkMailAddress;
    private String postNum;
    private String post1;
    private String post2;
    private String address;
    private String address1;
    private String address2;
    private String tell;
    private String birthday;
    private String creditNum;
    private String creditOwnerName;
    private String expirationDate;
    private String securityCode;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    //ユーザ名
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
//    	ひらがな
    public String getUserHurigana(){
    	return userHurigana;
    }
    public void setUserHurigana(String userHurigana){
    	this.userHurigana = userHurigana;
    }
    //パスワード
    public String getUserPassword() {
        return userPassword;
    }
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
    
    public String getcheckPassword() {
        return checkPassword;
    }
    public void setcheckPassword(String checkPassword) {
        this.checkPassword = checkPassword;
    }
    
    //メールアドレス
    public String getMailAddress() {
        return mailAddress;
    }
    public void setMailAddress(String mailAddress) {
        this.mailAddress = mailAddress;
    }
    public String getCheckMailAddress(){
    	return checkMailAddress;
    }
    public void setCheckMailAddress(String CheckMailAddress){
    	this.checkMailAddress = CheckMailAddress;
    }
    //郵便番号
    public String getPostNum() {
        return postNum;
    }
    public void setPostNum(String postNum) {
        this.postNum = postNum;
    }
    
    public String getPost1() {
        return post1;
    }
    public void setPost1(String post1) {
        this.post1 = post1;
    }
    
    public String getPost2() {
        return post2;
    }
    public void setPost2(String post2) {
        this.post2 = post2;
    }
    
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    
    public String getAddress1() {
        return address1;
    }
    public void setAddress1(String address1) {
        this.address1 = address1;
    }
    
    public String getAddress2() {
        return address2;
    }
    public void setAddress2(String address2) {
        this.address2 = address2;
    }
    
    public String getTell() {
        return tell;
    }
    public void setTell(String tell) {
        this.tell = tell;
    }
    
    //生年月日
    public String getBirthday() {
        return birthday;
    }
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
    
    //クレジットカード番号
    public String getCreditNum() {
        return creditNum;
    }
    public void setCreditNum(String creditNum) {
        this.creditNum = creditNum;
    }
    
    //カード名義人
    public String getCreditOwnerName() {
        return creditOwnerName;
    }
    public void setCreditOwnerName(String creditOwnerName) {
        this.creditOwnerName = creditOwnerName;
    }
    //有効期限
    public String getExpirationDate() {
        return expirationDate;
    }
    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }
    //セキュリティコード
    public String getSecurityCode() {
        return securityCode;
    }
    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }
}
