package jp.co.mmr.dto;

public class PurchaseDTO {
    //アクセサメソッド用変数定義
    private int id;
    private int rentalId;
    private int merchandiseId;
    private int statusId;
    private int originId;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    //レンタルID
    public int getRentalId() {
        return rentalId;
    }
    public void setRentalId(int rentalId) {
        this.rentalId = rentalId;
    }
    
    //商品ID
    public int getMerchandiseId() {
        return merchandiseId;
    }
    public void setMerchandiseId(int merchandiseId) {
        this.merchandiseId = merchandiseId;
    }
    
    //ステータス
    public int getStatusId() {
        return statusId;
    }
    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }
    
    //個体識別番号
    public int getOriginId() {
        return originId;
    }
    public void setOriginId(int originId) {
        this.originId = originId;
    }
}