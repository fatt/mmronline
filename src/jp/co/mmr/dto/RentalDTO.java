package jp.co.mmr.dto;

import java.sql.Date;
import java.sql.Timestamp;

public class RentalDTO {
    //�A�N�Z�T���\�b�h�p�ϐ���`
    private int id;
    private int userId;
    private Timestamp recipetDate;
    private Date deliveryDate;
    private int deliveryTime;
    private Date  returnDeadline;
    private Date returnDate;
    private int orderQuantity;
    private String lendChargeId;
    private String returnChargeId;
    private int statusId;

	//�����ԍ�
    public int getId() {
        return id;
    }

    public void setId(int orerId) {
        this.id = orerId;
    }

    //���[�UID

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    //������t����
    public Timestamp getRecipetDate() {
        return recipetDate;
    }
    public void setRecipetDate(Timestamp recipetDate) {
        this.recipetDate = recipetDate;
    }

    //���͂��\���
    public Date getDeliveryDate() {
        return deliveryDate;
    }
    public void setDeliveryDate(Date delivaryDate) {
        this.deliveryDate = delivaryDate;
    }

    public int getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(int deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    //�ԋp�\���
    public Date getReturnDeadline() {
        return returnDeadline;
    }
    public void setReturnDeadline(Date returnDeadline) {
        this.returnDeadline = returnDeadline;
    }

    //�ԋp��
    public Date getReturnDate() {
        return returnDate;
    }
    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    //������
    public int getOrderQuantity() {
        return orderQuantity;
    }
    public void setOrderQuantity(int orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    //�ݏo�S����
    public String getLendChargeId() {
        return lendChargeId;
    }
    public void setLendChargeId(String lendChargeId) {
        this.lendChargeId = lendChargeId;
    }

    //�ԋp�S����
    public String getReturnChargeId() {
        return returnChargeId;
    }
    public void setReturnChargeId(String returnChargeId) {
        this.returnChargeId = returnChargeId;
    }

    //�X�e�[�^�X�i��t�E�ݏo�E�ؔ[�E�ԋp�j
    public int getStatusId() {
        return statusId;
    }
    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

}