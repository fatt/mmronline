package jp.co.mmr.dto;

import java.io.InputStream;

public class MerchandiseDTO {
	int id;
	String name;
	InputStream image;
	int categoryid;
	int junleid;
	int artistid;
	int price;
	int arrivalid;
	int originnum;
	int featureid;

	public MerchandiseDTO() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

//	public MerchandiseDTO(int id, String name, blob image, int categoryid, int junleid, int artistid, int price,
//			int arrivalid, int originnum, int featureid) {
//		super();
//		this.id = id;
//		this.name = name;
//		this.image = image;
//		this.categoryid = categoryid;
//		this.junleid = junleid;
//		this.artistid = artistid;
//		this.price = price;
//		this.arrivalid = arrivalid;
//		this.originnum = originnum;
//		this.featureid = featureid;
//
//
//	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public InputStream getImage() {
		return image;
	}
	public void setImage(InputStream image) {
		this.image = image;
	}
	public int getCategoryid() {
		return categoryid;
	}
	public void setCategoryid(int categoryid) {
		this.categoryid = categoryid;
	}
	public int getJunleid() {
		return junleid;
	}
	public void setJunleid(int junleid) {
		this.junleid = junleid;
	}
	public int getArtistid() {
		return artistid;
	}
	public void setArtistid(int artistid) {
		this.artistid = artistid;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getArrivalid() {
		return arrivalid;
	}
	public void setArrivalid(int arrivalid) {
		this.arrivalid = arrivalid;
	}
	public int getOriginnum() {
		return originnum;
	}
	public void setOriginnum(int originnum) {
		this.originnum = originnum;
	}
	public int getFeatureid() {
		return featureid;
	}
	public void setFeatureid(int featureid) {
		this.featureid = featureid;
	}


}

