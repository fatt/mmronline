package jp.co.mmr.servlet.account;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.mmr.dao.UserDAO;
import jp.co.mmr.dto.UserDTO;
import jp.co.mmr.util.DataSourceManager;


@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String userId = (String)req.getParameter("login-id");
	    String userPassword = (String)req.getParameter("login-password");
	    try(Connection con = DataSourceManager.getConnection()) {
            UserDAO dao = new UserDAO(con);
            UserDTO user = dao.getUserByLogInfo(userId, userPassword);
            if (user != null) {
                session.setAttribute("loginUser", user);
            } else {
                session.setAttribute("loginErrMsg", "ID又はパスワードに誤りがあります。");
            }
        } catch (Exception e) {
            e.printStackTrace();
            resp.sendRedirect("error.jsp");
        }
	    // 別画面からホームに遷移したときにエラーメッセージ表示したくないからフラグを用意
        session.setAttribute("fromLoginFlag", true);
	    resp.sendRedirect("index.jsp");
	}

}
