package jp.co.mmr.servlet.account;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.mmr.dao.UserDAO;
import jp.co.mmr.dto.UserDTO;
import jp.co.mmr.util.DataSourceManager;


@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	    HttpSession session = req.getSession();
	    session.removeAttribute("loginUser");
	    session.removeAttribute("cartItem");
	    resp.sendRedirect("index.jsp");
	}
	
}
