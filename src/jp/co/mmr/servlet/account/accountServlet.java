package jp.co.mmr.servlet.account;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.mmr.dao.UserDAO;
import jp.co.mmr.dto.UserAddDTO;
import jp.co.mmr.util.DataSourceManager;
import jp.co.mmr.util.Validate;


@WebServlet("/account")
public class accountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
//			URLで連結された場合、ログイン画面に転送する
			response.sendRedirect("index.jsp");
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
//        入力されたデータの値をとる	
		request.setCharacterEncoding("UTF-8");
        String mailAddress = request.getParameter("mailAddress");
	    String mailforCheck = request.getParameter("forCheck");
	    String name_1 = request.getParameter("name_1");
	    String name_2 = request.getParameter("name_2");
	    String password = request.getParameter("password");
	    String passwordForCheck = request.getParameter("passwordForCheck");
	    String postNum_1 = request.getParameter("postNum_1");
	    String postNum_2 = request.getParameter("postNum_2");
	    String address_1 = request.getParameter("address_1");
	    String address_2 = request.getParameter("address_2");
	    String telNum = request.getParameter("telNum");
	    String brithYear = request.getParameter("brithYear");
	    String brithMonth = request.getParameter("brithMonth");
	    String brithDay = request.getParameter("brithDay");
	    String ownerName = request.getParameter("ownerName");
	    String cardNum_1 = request.getParameter("cardNum_1");
	    String cardNum_2 = request.getParameter("cardNum_2");
	    String cardNum_3 = request.getParameter("cardNum_3");
	    String cardNum_4 = request.getParameter("cardNum_4");
	    String validatePeriod = request.getParameter("validatePeriod");
	    String securityCode = request.getParameter("securityCode");
	    
//	    ばらばらになったデータをまとめる
	    String post_number = postNum_1 + postNum_2 ;
	    String address = address_1 + address_2;
	    String birthday = brithYear + brithMonth + brithDay;
	    String credit_num = cardNum_1 + cardNum_2 + cardNum_3 + cardNum_4 ;
	 

//			入力項目検証
			if(!mailAddress.equals(mailforCheck)){
				request.setAttribute("mail_error_message","メールアドレスと確認が一致していません、もう一度確認してください");
			}
			if(Validate.isNumber(name_1)){
				request.setAttribute("name_error_message","お名前を正しく入力してください");
			}
			if(Validate.isNumber(name_2)){
				request.setAttribute("name_error_message","フリガナを正しく入力してください");
			}
			if(!password.equals(passwordForCheck)){
				request.setAttribute("password_error_message","パスワードと確認が一致していません、もう一度確認してください");
			}
			if((password.length() < 8)||(password.length() >20)){
				request.setAttribute("password_error_message","パスワードは8文字以上、20文字以下で入力してください");
			}

	        //半角英数字　判定するパターン
	        Pattern p = Pattern.compile("^[0-9a-zA-Z]+$");
	        Matcher m = p.matcher(password);
	        if(m.find() == false){
	        	request.setAttribute("password_error_message","パスワードは半角英数で入力してください");
	        }
	        if(telNum.length() > 11){
	        	request.setAttribute("tell_error_message","電話番号は11桁まで入力してください");
	        }
	        if(brithYear.length() != 4){
	        	request.setAttribute("birthday_error_message","生年月日の年は西暦年の４桁で入力してください");
	        }
	        if(brithMonth.length() != 2 ){
	        	request.setAttribute("birthday_error_message","生年月日の月は２桁で入力してください");
	        }
	        if(brithDay.length() != 2 ){
	        	request.setAttribute("birthday_error_message","生年月日の月は２桁で入力してください");
	        }
	        //半角英文字　判定するパターン
	        Pattern e = Pattern.compile("^[a-zA-Z]+$");
	        Matcher em = e.matcher(ownerName);
	        if(em.find() == false){
	        	request.setAttribute("card_error_message","カード名義人は半角英文字で入力してください");
	        }
	        if((cardNum_1.length() != 4 )||(cardNum_2.length() != 4)||(cardNum_3.length() != 4)||(cardNum_4.length() != 4)){
	        	request.setAttribute("card_error_message","カード番号は16桁の数字で入力してください");
	        }
	        if(!Validate.isNumber(cardNum_1)||!Validate.isNumber(cardNum_2)||!Validate.isNumber(cardNum_3)||!Validate.isNumber(cardNum_4)){
	        	request.setAttribute("card_error_message","カード番号は16桁の数字で入力してください");
	        }
	        if(validatePeriod.length() != 6){
	        	request.setAttribute("card_error_message","カードの有効期限はYYYYMMの形で6桁の数字で入力してください");
	        }
	        if(securityCode.length() != 3){
	        }
	        if((request.getAttribute("mail_error_message") != null) 
	        		|| (request.getAttribute("password_error_message") != null) 
	        		|| (request.getAttribute("birthday_error_message") != null)
	        		|| (request.getAttribute("card_error_message") != null)){
	        	request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp").forward(request, response);
	        }else{

//	    		DBとのconnectionをとる

	    	try(Connection conn = DataSourceManager.getConnection()) {
	    		
//	    		入力値を文字化にする処理 + DAOと連結してconnectionを代入する
	    		UserDAO dao = new UserDAO(conn);

		        UserAddDTO user = new UserAddDTO();
				user.setUserName((Validate.HTMLEscape(Validate.SQLEscape(name_1))));
				user.setUserHurigana((Validate.HTMLEscape(Validate.SQLEscape(name_2))));
				user.setUserPassword((Validate.HTMLEscape(Validate.SQLEscape(password))));
				user.setcheckPassword((Validate.HTMLEscape(Validate.SQLEscape(passwordForCheck))));
				user.setMailAddress((Validate.HTMLEscape(Validate.SQLEscape(mailAddress))));
				user.setCheckMailAddress((Validate.HTMLEscape(Validate.SQLEscape(mailforCheck))));
				user.setPost1((Validate.HTMLEscape(Validate.SQLEscape(postNum_1))));
				user.setPost2((Validate.HTMLEscape(Validate.SQLEscape(postNum_2))));
				user.setPostNum((Validate.HTMLEscape(Validate.SQLEscape(post_number))));
				user.setAddress((Validate.HTMLEscape(Validate.SQLEscape(address))));
				user.setTell((Validate.HTMLEscape(Validate.SQLEscape(telNum))));
				user.setBirthday((Validate.HTMLEscape(Validate.SQLEscape(birthday))));
				user.setCreditOwnerName((Validate.HTMLEscape(Validate.SQLEscape(ownerName))));
				user.setCreditNum((Validate.HTMLEscape(Validate.SQLEscape(credit_num))));
				user.setExpirationDate((Validate.HTMLEscape(Validate.SQLEscape(validatePeriod))));
				user.setSecurityCode((Validate.HTMLEscape(Validate.SQLEscape(securityCode))));
		        			
				//　セッションを取得する
				HttpSession session = request.getSession();
		        session.setAttribute("add_user", user);
		        
		        //DAOを通してデータベースに追加する　成功すると
		        if(dao.addUser(user)){
		        	request.setAttribute("addUser_message","会員登録が成功しました");
		        }else{
		        	request.setAttribute("addUser_message","会員登録が失敗しました");
		        };      
		        
		        request.getRequestDispatcher("/WEB-INF/jsp/home.jsp").forward(request, response);
		        
		        return;
			}catch (SQLException | NamingException ee) {
				ee.printStackTrace();
				response.sendRedirect("error.jsp");
			}
	   }
	}
}
