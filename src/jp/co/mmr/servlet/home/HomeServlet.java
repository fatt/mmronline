package jp.co.mmr.servlet.home;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.mmr.dao.FeatureDAO;
import jp.co.mmr.dao.MerchandiseDAO;
import jp.co.mmr.dao.PurchaseDAO;
import jp.co.mmr.dao.RentalDAO;
import jp.co.mmr.dto.FeatureDTO;
import jp.co.mmr.dto.MerchandiseDTO;
import jp.co.mmr.dto.PurchaseDTO;
import jp.co.mmr.dto.RentalDTO;
import jp.co.mmr.dto.UserDTO;
import jp.co.mmr.util.DataSourceManager;


@WebServlet("/home")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	    HttpSession session = req.getSession();
	    UserDTO user = (UserDTO)session.getAttribute("loginUser");
	    
	    // メッセージ処理
	    if (user != null && session.getAttribute("fromLoginFlag") != null) {
	        // ログイン成功時(sessionにloginUserが存在し、LoginServletからの遷移の場合)
	        session.removeAttribute("loginErrMsg");
            req.setAttribute("loginSucMsg", "ログインに成功しました。ようこそ" + user.getUserName() + "さん！");
        } else if (user == null && session.getAttribute("fromLoginFlag") != null) {
            // ログイン失敗時(sessionにloginUserが存在せず、LoginServletからの遷移の場合)
        } else if (session.getAttribute("fromLoginFlag") == null) {
            session.removeAttribute("loginErrMsg");
        }
	    //  HomeServlet通るたびにフラグをたたむ
	    session.removeAttribute("fromLoginFlag");
	    
	    // データ取得処理
	    try (Connection con = DataSourceManager.getConnection()) {
	        // 特集の取得
	        FeatureDAO featureDao = new FeatureDAO(con);
	    	Calendar calendar = Calendar.getInstance();
	    	int period = (calendar.get(Calendar.YEAR) * 100) + (calendar.get(Calendar.MONTH) + 1);
	        ArrayList<FeatureDTO> list = featureDao.getMonthlyFeatureList(period);
	        req.setAttribute("features", list);
	        
	        // ログインしている場合、注文履歴を取得
	        if (user != null) {
	            RentalDAO rentalDao = new RentalDAO(con);
	            ArrayList<RentalDTO> rentalList = rentalDao.getRentalListByUserIdWithReceiptSort(user.getId(), false);
	            PurchaseDAO purchaseDao = new PurchaseDAO(con);
	            ArrayList<PurchaseDTO> purchaseList = new ArrayList<>();
	            // 購入明細リストを全て取得するまでループ　誰か助けて
	            for (RentalDTO rental : rentalList) {
	                purchaseList.addAll(purchaseDao.getPurchaseListByRentalId(rental.getId()));
	            }
	            //取得した各購入明細に対応する商品データリストを生成
	            MerchandiseDAO merchandiseDAO = new MerchandiseDAO(con);
	            ArrayList<MerchandiseDTO> merchandiseList = new ArrayList<>();
	            for (PurchaseDTO purchase : purchaseList) {
	                merchandiseList.add(merchandiseDAO.getMerchandiseById(purchase.getMerchandiseId()));
	            }
	            req.setAttribute("merchandise_history", merchandiseList);
            }
	        
	        req.getRequestDispatcher("/WEB-INF/jsp/home.jsp").forward(req, resp);
	    } catch (SQLException | NamingException e) {
            e.printStackTrace();
            resp.sendRedirect("error.jsp");
        }
	}

}
