package jp.co.mmr.servlet.search;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.mmr.dao.MerchandiseDAO;
import jp.co.mmr.dto.MerchandiseDTO;
import jp.co.mmr.util.DataSourceManager;
import jp.co.mmr.util.Validate;

/**
 * Servlet implementation class SearchServlet
 */
@WebServlet("/searchMerchandise")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		int categoryNo = 0;
		int junleNo = 0;

		try (Connection con = DataSourceManager.getConnection()) {
			MerchandiseDAO dao = new MerchandiseDAO(con);
			ArrayList<MerchandiseDTO> list = new ArrayList<MerchandiseDTO>();

			req.setCharacterEncoding("UTF-8");

			// カテゴリNoを文字列型から数値型へ変換
			String cateNo = req.getParameter("searchCategory");
			if (!("".equals(cateNo) || (null == cateNo))) {
				categoryNo = Integer.parseInt(cateNo);
			}

			// ジャンルNoを文字列型から数値型へ変換
			String junNo = req.getParameter("searchJunle");
			if (junNo != null) {
				junleNo = Integer.parseInt(junNo);
			}

			// 商品名を受け取る
			String itemName = req.getParameter("searchName");

			// カテゴリが選択されておらず、商品名が入力されている場合
			if ((((categoryNo != 1) || (categoryNo != 2) || (categoryNo != 3)) && (junleNo == 0))
					&& !(("".equals(itemName) )||( itemName == null))) {

				//HTMLチェック
				String htmlCheckedName =Validate.HTMLEscape(itemName);
				list = dao.searchName(htmlCheckedName);


				// 商品名が入力されていない場合
			} else if ((junleNo != 0) && ("".equals(itemName) || itemName == null)) {

				list = dao.cateJunNo(categoryNo, junleNo);

				// カテゴリのみ入力されている場合
			} else if ((((categoryNo == 1) || (categoryNo == 2) || (categoryNo == 3) || (categoryNo == 0)) && (junleNo == 0))
					&& ("".equals(itemName) || itemName == null)) {

				list = dao.cateNo(categoryNo);

				// すべて未入力で検索した場合
			} else if ((((categoryNo != 1) || (categoryNo != 2) || (categoryNo != 3)) && (junleNo == 0))
					&& (("".equals(itemName))||( itemName == null))){

				list = null;

			} else {
				// カテゴリ、ジャンル、商品名が入力されている場合

				//HTMLエスケープ処理
				String htmlCheckedName =Validate.HTMLEscape(itemName);
				list = dao.searchItem(categoryNo, junleNo, htmlCheckedName);
			}

			int featureId = req.getParameter("feature_id") != null ? Validate.convertStrToInteger(req.getParameter("feature_id")) : 0;
			req.setAttribute("featureId", featureId);
			req.setAttribute("fromHome", true);	// Homeからの遷移かのフラグ
			req.setAttribute("categoryNo", categoryNo);
			req.setAttribute("junleNo", junleNo);
			req.setAttribute("itemName", itemName);
			req.setAttribute("itemList", list);

		} catch (NumberFormatException | SQLException | NamingException e) {
			e.printStackTrace();
			req.getParameter("header_error_message");
		}
		req.getRequestDispatcher("/WEB-INF/jsp/search.jsp").forward(req, res);
	}

}
