package jp.co.mmr.servlet.search;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.mmr.dao.MerchandiseDAO;
import jp.co.mmr.dto.MerchandiseDTO;
import jp.co.mmr.util.DataSourceManager;
import jp.co.mmr.util.Validate;

/**
 * Servlet implementation class MerchandiseDetailServlet
 */
@WebServlet("/merchandiseDetail")
public class MerchandiseDetailServlet extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		res.setContentType("text/plain; charset=UTF-8");

		String itemno = req.getParameter("itemno");
		Integer itemNo = Validate.convertStrToInteger(itemno);


		try (Connection con = DataSourceManager.getConnection()) {
			MerchandiseDAO dao = new MerchandiseDAO(con);
			MerchandiseDTO dto = dao.getMerchandiseById(itemNo);
			req.setAttribute("itemdto", dto);

		} catch (SQLException | NamingException | NullPointerException e) {
			e.printStackTrace();
			req.getRequestDispatcher("error.jsp").forward(req, res);
		}
		req.getRequestDispatcher("/WEB-INF/jsp/itemDetail.jsp").forward(req, res);
	}


	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

	}

}
