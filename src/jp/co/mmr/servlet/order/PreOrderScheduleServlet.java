package jp.co.mmr.servlet.order;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.mmr.dto.StringMerchandiseDTO;

/**
 * Servlet implementation class PreOrderScheduleServlet
 */
@WebServlet("/preorderschedule")
public class PreOrderScheduleServlet extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

	}





	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		HttpSession session = req.getSession();
		if(session == null){
			req.getRequestDispatcher("/WEB-INF/jsp/regist.jsp").forward(req, res);
		}

			ArrayList<StringMerchandiseDTO> list = (ArrayList<StringMerchandiseDTO>)session.getAttribute("cartItem");
			if(list.size() == 0){
				req.getRequestDispatcher("/WEB-INF/jsp/cart.jsp").forward(req, res);
			}
			session.getAttribute("count");

			req.getRequestDispatcher("/WEB-INF/jsp/deliveryDate.jsp").forward(req, res);


	}

}
