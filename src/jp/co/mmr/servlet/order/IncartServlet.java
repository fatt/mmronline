package jp.co.mmr.servlet.order;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.mmr.dto.StringMerchandiseDTO;
import jp.co.mmr.util.Validate;

/**
 * Servlet implementation class IncartServlet
 */
@WebServlet("/incart")
public class IncartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		HttpSession session = req.getSession();

		// if(session.getAttribute("cartItem") == null){
		// req.getRequestDispatcher("/WEB-INF/jsp/home.jsp").forward(req, res);
		// }
		ArrayList<StringMerchandiseDTO> list = (ArrayList<StringMerchandiseDTO>) session.getAttribute("cartItem");
		if (list == null) {
			res.sendRedirect("index.jsp");
		} else {
			req.getRequestDispatcher("/WEB-INF/jsp/cart.jsp").forward(req, res);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		HttpSession session = req.getSession();

		req.setCharacterEncoding("UTF-8");
		String id = req.getParameter("delete");
		Integer listid = Validate.convertStrToInteger(id);
		int count = 0;
		try {
			ArrayList<StringMerchandiseDTO> list = (ArrayList<StringMerchandiseDTO>) session.getAttribute("cartItem");

			list.remove(listid.intValue());

			session.setAttribute("cartItem", list);

			count = list.size();
			int sum = 0;
			for (int i = 0; i < list.size(); i++) {
				StringMerchandiseDTO dto = list.get(i);
				sum += dto.getPrice();
			}
			if (list.size() == 0) {
				session.removeAttribute("cartItem");
				res.sendRedirect("index.jsp");
			} else {
				session.setAttribute("price", sum);
				session.setAttribute("count", count);
				req.getRequestDispatcher("/WEB-INF/jsp/cart.jsp").forward(req, res);
			}

		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			session.removeAttribute("cartItem");
			session.removeAttribute("price");
			session.removeAttribute("count");
			res.sendRedirect("index.jsp");
		}

	}

}
