package jp.co.mmr.servlet.order;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.mmr.dao.MerchandiseDAO;
import jp.co.mmr.dto.MerchandiseDTO;
import jp.co.mmr.dto.StringMerchandiseDTO;
import jp.co.mmr.util.Converter;
import jp.co.mmr.util.DataSourceManager;
import jp.co.mmr.util.Validate;

/**
 * Servlet implementation class CartServlet
 */
@WebServlet("/cart")
@SuppressWarnings("unchecked")
public class CartServlet extends HttpServlet {
	private final String REQUEST_STRING = "merchandiseId";

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {


	}


	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {


		HttpSession session = req.getSession();
		req.setCharacterEncoding("UTF-8");
		String id = req.getParameter(REQUEST_STRING);
		Integer itemid = Validate.convertStrToInteger(id);


		try (Connection con = DataSourceManager.getConnection()) {
			MerchandiseDAO dao = new MerchandiseDAO(con);
			MerchandiseDTO dto = dao.getMerchandiseById(itemid);
			StringMerchandiseDTO sdto = new StringMerchandiseDTO();
			sdto.setId(dto.getId());
			sdto.setName(dto.getName());
			sdto.setImage(dto.getImage());
			sdto.setPrice(dto.getPrice());
			sdto.setOriginnum(dto.getOriginnum());
			sdto.setFeatureid(dto.getFeatureid());
			int cate = dto.getCategoryid();
			sdto.setCategoryid(Converter.getCategoryByCategoryId(cate));
			int junle = dto.getJunleid();
			sdto.setJunleid(Converter.getJunleByJunleId(junle));
			int artist = dto.getArtistid();
			sdto.setArtistid(Converter.getArtistByArtistId(artist));
			int arrival = dto.getArrivalid();
			sdto.setArrivalid(Converter.getArrivalByArrivalId(arrival));
			int count = 0;

			try {
				if (session.getAttribute("cartItem") == null) {
					ArrayList<StringMerchandiseDTO> list = new ArrayList<StringMerchandiseDTO>();
					list.add(sdto);
					session.setAttribute("cartItem", list);
					count = list.size();
					int price = sdto.getPrice();
					session.setAttribute("price", price);
				} else {
					ArrayList<StringMerchandiseDTO> list = (ArrayList<StringMerchandiseDTO>) session.getAttribute("cartItem");
					list.add(sdto);
					count = list.size();
					int sum = 0;
					for (int i = 0; i < list.size(); i++) {
						sdto = list.get(i);
						sum += sdto.getPrice();
					}
					session.setAttribute("price", sum);

				}

				session.setAttribute("count", count);
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		}

		req.getRequestDispatcher("/WEB-INF/jsp/search.jsp").forward(req, res);

	}

}
