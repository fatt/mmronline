package jp.co.mmr.servlet.order;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.mmr.dao.PurchaseDAO;
import jp.co.mmr.dao.RentalDAO;
import jp.co.mmr.dto.PurchaseDTO;
import jp.co.mmr.dto.RentalDTO;
import jp.co.mmr.dto.StringMerchandiseDTO;
import jp.co.mmr.dto.UserDTO;
import jp.co.mmr.util.DataSourceManager;

/**
 * Servlet implementation class OrderScheduleServlet
 */
@WebServlet("/orderSchedule")
public class OrderScheduleServlet extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.sendRedirect("index.jsp");
	}

	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		HttpSession session = req.getSession();
		UserDTO dto = (UserDTO) session.getAttribute("loginUser");

		if (dto != null) {

			int userid = dto.getId();
			try (Connection con = DataSourceManager.getConnection()) {
			ArrayList<StringMerchandiseDTO> list = (ArrayList<StringMerchandiseDTO>) session.getAttribute("cartItem");
			int count = list.size();

			req.setCharacterEncoding("UTF-8");

				String sdate = req.getParameter("Date");


				// 現在時刻生成 java.sql.timestamp
				java.sql.Timestamp time1 = new Timestamp(System.currentTimeMillis());

//				if((sdate != "") || (sdate != "date")){
				Date date = Date.valueOf(sdate);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(date);
				calendar.add(Calendar.DATE, 7);
				java.util.Date returndate = calendar.getTime();
				Date returndate2 = new java.sql.Date(returndate.getTime());

				String stime = req.getParameter("time");
				int time = Integer.parseInt(stime);



				RentalDAO dao = new RentalDAO(con);
				int status = 1;
				RentalDTO rentaldto = new RentalDTO();
				rentaldto.setUserId(userid);
				rentaldto.setRecipetDate(time1);
				rentaldto.setDeliveryDate(date);
				rentaldto.setDeliveryTime(time);
				rentaldto.setReturnDeadline(returndate2);
				rentaldto.setOrderQuantity(count);
				rentaldto.setStatusId(status);

				int result = dao.setRental(rentaldto);
				if (result != 1) {
					req.getRequestDispatcher("error.jsp").forward(req, res);
				}

				int orderId = dao.searchId(rentaldto);
				rentaldto.setId(orderId);

				for (int i = 0; i < list.size(); i++) {
					StringMerchandiseDTO dto2 = list.get(i);
					int Merchandiseid = dto2.getId();
					PurchaseDTO purdto = new PurchaseDTO();
					purdto.setRentalId(orderId);
					purdto.setMerchandiseId(Merchandiseid);
					purdto.setStatusId(status);
					PurchaseDAO purdao = new PurchaseDAO(con);
					int resultset = purdao.setPurchase(purdto);
					if (resultset != 1) {
						req.getRequestDispatcher("error.jsp").forward(req, res);
					}
				}

				session.setAttribute("rentalSchedule", rentaldto);

				req.getRequestDispatcher("/WEB-INF/jsp/oderCompllete.jsp").forward(req, res);
//				}else{
//					req.getRequestDispatcher("error.jsp").forward(req, res);
//				}

			} catch (SQLException | NamingException | NullPointerException e) {
				e.printStackTrace();
				res.sendRedirect("index.jsp");
			}

		} else {
			res.sendRedirect("index.jsp");
		}

	}

}
