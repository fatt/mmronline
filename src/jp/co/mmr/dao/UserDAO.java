package jp.co.mmr.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.mmr.dto.UserAddDTO;
import jp.co.mmr.dto.UserDTO;

/**
 * ユーザデータDAOクラス
 * @author RyosukeOmori
 */
public class UserDAO {
    
    private Connection con;
    
    public UserDAO(Connection con) {
        this.con = con;
    }
    
    /**
     * 引数に与えられたメールアドレスとパスワードに該当するUserDTOを取得する
     * @param mail
     * @param password
     * @return
     */
    public UserDTO getUserByLogInfo(String mail, String password) throws SQLException {
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT");
        sb.append("        *");
        sb.append("    FROM");
        sb.append("        USER");
        sb.append("    WHERE");
        sb.append("        mail_address = ?");
        sb.append("        AND password = ?");
        try(PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
            ps.setString(1, mail);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                UserDTO dto = new UserDTO();
                dto.setId(rs.getInt("ID"));
                dto.setUserName(rs.getString("NAME"));
                dto.setUserPassword(rs.getString("PASSWORD"));
                dto.setMailAddress(rs.getString("MAIL_ADDRESS"));
                dto.setPostNum(rs.getInt("POST_NUMBER"));
                dto.setAddress(rs.getString("ADDRESS"));
                dto.setTell(rs.getString("TELL"));
                dto.setBirthday(rs.getDate("BIRTHDAY"));
                dto.setCreditNum(rs.getString("CREDIT_NUM"));
                dto.setCreditOwnerName(rs.getString("CREDIT_OWNER"));
                dto.setExpirationDate(rs.getInt("EXPIRATION_DATE"));
                dto.setSecurityCode(rs.getInt("SECURITY_CODE"));
                return dto;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
        return null;
    }
    
    /**
	 * EMP表にデータを追加する。
	 * 正常に追加できた場合はtrueを返す。
	 * @param data 追加するEmpデータ
	 * @return 追加成功すればtrue
	 * @throws SQLException 
	 */
	public boolean addUser(UserAddDTO add_user) throws SQLException{
		
		// SQL文の作成
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        USER(name, hurigana, password, mail_address, post_number, address, tell, birthday, credit_num, credit_owner, expiration_date, security_code)");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("        )");

		try(PreparedStatement ps = con.prepareStatement(sb.toString())){
			
			// ?に値をセットして実行
			ps.setString(1, add_user.getUserName());
			ps.setString(2, add_user.getUserHurigana());
			ps.setString(3, add_user.getUserPassword());
			ps.setString(4, add_user.getMailAddress());
			ps.setString(5, add_user.getPostNum());
			ps.setString(6, add_user.getAddress());
			ps.setString(7, add_user.getTell());
			ps.setString(8, add_user.getBirthday());
			ps.setString(9, add_user.getCreditNum());
			ps.setString(10, add_user.getCreditOwnerName());
			ps.setString(11, add_user.getExpirationDate());
			ps.setString(12, add_user.getSecurityCode());
			
			int result = ps.executeUpdate();
			
			// 成功すればtrueを返す
			if(result == 1){
				return true;
			}
			
			// 失敗なのでfalseを返す
			return false;
			
		} catch (SQLException e) {
			throw e;
		}
	}
}
