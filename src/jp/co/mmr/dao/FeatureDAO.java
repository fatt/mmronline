package jp.co.mmr.dao;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import jp.co.mmr.dto.FeatureDTO;

/**
 * ���W�f�[�^DAO�N���X
 * @author RyosukeOmori
 *
 */
public class FeatureDAO {

    private Connection con;

    public FeatureDAO(Connection con) {
        this.con = con;
    }

    /**
     * Feature�e�[�u�����ɂ���S�Ă̗v�f��FeatureDTO�I�u�W�F�N�g�Ƃ��Ď擾����B
     * @return �擾����FeatureDTO���X�g
     * @throws SQLException
     */
    public ArrayList<FeatureDTO> getAllFeatureList() throws SQLException {
        StringBuffer sb = new StringBuffer();
        sb.append("select * from feature");

        ArrayList<FeatureDTO> list = new ArrayList<>();
        try(PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                FeatureDTO featureDto = new FeatureDTO();
                featureDto.setId(rs.getInt("ID"));
                featureDto.setTitle(rs.getString("TITLE"));
                Blob img = rs.getBlob("IMAGE");
                if (img != null) {
                    featureDto.setImage(img.getBinaryStream());
                }
                featureDto.setPeriod(rs.getInt("PERIOD"));
                list.add(featureDto);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * �����ɓn���ꂽ�w�茎�ɊY������FeatureDTO��Feature�e�[�u������擾����
     * ex) list = dao.getMonthlyFeatureList(201706); //2017�N6���̓��W���擾
     * @param period �擾���������W�f�[�^�̊���
     * @return �����ɊY������FeatureDTO���X�g
     * @throws SQLException
     */
    public ArrayList<FeatureDTO> getMonthlyFeatureList(int period) throws SQLException {
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT");
        sb.append("        *");
        sb.append("    FROM");
        sb.append("        feature");
        sb.append("    WHERE");
        sb.append("        period = ?");

        ArrayList<FeatureDTO> list = new ArrayList<>();
        try(PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
            ps.setInt(1, period);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                FeatureDTO featureDto = new FeatureDTO();
                featureDto.setId(rs.getInt("ID"));
                featureDto.setTitle(rs.getString("TITLE"));
                Blob img = rs.getBlob("IMAGE");
                if (img != null) {
                    featureDto.setImage(img.getBinaryStream());
                }
                featureDto.setPeriod(rs.getInt("PERIOD"));
                list.add(featureDto);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * ���WID����Y��������WDTO���擾����
     * @param id �擾���������W��ID
     * @return �擾�������W�f�[�^����null
     * @throws SQLException
     */
    public FeatureDTO getFeatureById(int id) throws SQLException {
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT");
        sb.append("        *");
        sb.append("    FROM");
        sb.append("        feature");
        sb.append("    WHERE");
        sb.append("        id = ?");

        try(PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                FeatureDTO featureDto = new FeatureDTO();
                featureDto.setId(rs.getInt("ID"));
                featureDto.setTitle(rs.getString("TITLE"));
                Blob img = rs.getBlob("IMAGE");
                if (img != null) {
                    featureDto.setImage(img.getBinaryStream());
                }
                featureDto.setPeriod(rs.getInt("PERIOD"));
                return featureDto;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
        return null;
    }

    /**
     * ���WID����Y������摜�f�[�^���擾����
     * @param id �擾�������摜�f�[�^�̓��WID
     * @return �擾�����摜�f�[�^
     * @throws IOException
     * @throws SQLException
     */
    public BufferedImage getImageById(int id) throws IOException, SQLException {
        FeatureDTO dto = getFeatureById(id);
        InputStream is = dto.getImage();
        BufferedInputStream bis = new BufferedInputStream(is);
        return ImageIO.read(bis);
    }

    /**
     * Feature�e�[�u���Ɉ����ɓn�������WDTO���i�[����
     * @param dto �ǉ����������W�f�[�^
     * @return �ǉ�������1,�o���Ȃ����0
     * @throws SQLException
     */
    public int setFeature(FeatureDTO dto) throws SQLException {
        StringBuffer sb = new StringBuffer();
        sb.append("INSERT");
        sb.append("    INTO");
        sb.append("        feature(");
        sb.append("            title");
        sb.append("            ,image");
        sb.append("            ,period");
        sb.append("        )");
        sb.append("    VALUES");
        sb.append("        (");
        sb.append("            ?");
        sb.append("            ,?");
        sb.append("            ,?");
        sb.append("        )");

        try(PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
            ps.setString(1, dto.getTitle());
            ps.setBlob(2, dto.getImage());
            ps.setInt(3, dto.getPeriod());

            return ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
    }
    
    public int updateFeature(FeatureDTO dto) throws SQLException {
    	String sql = "update feature set title = ?, image = ?, period = ? where id = ?";

        try(PreparedStatement ps = this.con.prepareStatement(sql)) {
            ps.setString(1, dto.getTitle());
            ps.setBlob(2, dto.getImage());
            ps.setInt(3, dto.getPeriod());
            ps.setInt(4, dto.getId());

            return ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
    }

}
