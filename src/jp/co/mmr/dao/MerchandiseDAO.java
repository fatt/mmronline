package jp.co.mmr.dao;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import jp.co.mmr.dto.MerchandiseDTO;
import jp.co.mmr.util.Validate;

public class MerchandiseDAO {

	private Connection con;

	public MerchandiseDAO(Connection con) {
		this.con = con;
	}

	/**
	 * 取得したい商品を指定して取得する
	 *
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public MerchandiseDTO getMerchandiseById(int id) throws SQLException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        *");
		sb.append("    FROM");
		sb.append("        merchandise");
		sb.append("    WHERE");
		sb.append("        id = ?");
		try (PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				MerchandiseDTO dto = new MerchandiseDTO();
				dto.setId(rs.getInt("id"));
				dto.setName(rs.getString("name"));
				dto.setImage(rs.getBinaryStream("image"));
				dto.setCategoryid(rs.getInt("category_id"));
				dto.setJunleid(rs.getInt("junle_id"));
				dto.setArtistid(rs.getInt("artist_id"));
				dto.setPrice(rs.getInt("price"));
				dto.setArrivalid(rs.getInt("arrival_id"));
				dto.setOriginnum(rs.getInt("origin_num"));
				return dto;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
		return null;
	}

	/**
	 * 指定した商品の画像データのみを取得する
	 *
	 * @param id
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public BufferedImage getImageById(int id) throws IOException, SQLException {
		MerchandiseDTO dto = getMerchandiseById(id);
		InputStream is = dto.getImage();
		BufferedInputStream bis = new BufferedInputStream(is);
		return ImageIO.read(bis);
	}

	/**
	 * 引数に渡したMerchandiseDTOをDBに登録する id（auto_increment）,
	 * feature_id（商品追加時はないはず）は省略
	 *
	 * @param dto
	 * @return
	 * @throws SQLException
	 */
	public int setMerchandise(MerchandiseDTO dto) throws SQLException {
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT");
		sb.append("    INTO");
		sb.append("        merchandise(");
		sb.append("            name");
		sb.append("            ,image");
		sb.append("            ,category_id");
		sb.append("            ,junle_id");
		sb.append("            ,artist_id");
		sb.append("            ,price");
		sb.append("            ,arrival_id");
		sb.append("            ,origin_num");
		sb.append("        )");
		sb.append("    VALUES");
		sb.append("        (");
		sb.append("            ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("        )");
		try (PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
			ps.setString(1, dto.getName());
			ps.setBlob(2, dto.getImage());
			ps.setInt(3, dto.getCategoryid());
			ps.setInt(4, dto.getJunleid());
			ps.setInt(5, dto.getArtistid());
			ps.setInt(6, dto.getPrice());
			ps.setInt(7, dto.getArrivalid());
			ps.setInt(8, dto.getOriginnum());
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
	}

	public ArrayList<MerchandiseDTO> getMerchandiseByFeatureId(int featureId) throws SQLException {
		String sql = "select feature_target.feature_id, merchandise.id, merchandise.name, merchandise.image, merchandise.category_id, merchandise.junle_id, merchandise.artist_id, merchandise.price, merchandise.arrival_id, merchandise.origin_num from feature_target inner join merchandise on feature_target.merchandise_id = merchandise.id where feature_target.feature_id = ?";
		try (PreparedStatement ps = this.con.prepareStatement(sql)) {
			ps.setInt(1, featureId);
			ResultSet rs = ps.executeQuery();
			ArrayList<MerchandiseDTO> list = new ArrayList<>();
			while (rs.next()) {
				MerchandiseDTO dto = new MerchandiseDTO();
				dto.setId(rs.getInt("id"));
				dto.setName(rs.getString("name"));
				dto.setImage(rs.getBinaryStream("image"));
				dto.setCategoryid(rs.getInt("category_id"));
				dto.setJunleid(rs.getInt("junle_id"));
				dto.setArtistid(rs.getInt("artist_id"));
				dto.setPrice(rs.getInt("price"));
				dto.setArrivalid(rs.getInt("arrival_id"));
				dto.setOriginnum(rs.getInt("origin_num"));
				list.add(dto);
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
	}

	// 蠑墓焚縺ｧ蜿励￠蜿悶▲縺溘き繝�繧ｴ繝ｪ縲√ず繝｣繝ｳ繝ｫ縲∝膚蜩∝錐縺ｧ讀懃ｴ｢繧定｡後≧繝｡繧ｽ繝�繝�
	public ArrayList<MerchandiseDTO> searchItem(int cateNo, int junNo, String itemname) throws SQLException {

		ArrayList<MerchandiseDTO> list = new ArrayList<MerchandiseDTO>();

		// SQL譁�縺ｮ菴懈��
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        *");
		sb.append("    FROM");
		sb.append("        merchandise");
		sb.append("    WHERE");
		sb.append("        category_id = ?");
		sb.append("        AND junle_id = ?");
		sb.append("        AND name LIKE ?");

		try (PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
			ps.setInt(1, cateNo);
			ps.setInt(2, junNo);
			ps.setString(3, itemname + "%");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				MerchandiseDTO dto = new MerchandiseDTO();
				dto.setId(rs.getInt("id"));
				dto.setName(rs.getString("name"));
				dto.setImage(rs.getBinaryStream("image"));
				dto.setCategoryid(rs.getInt("category_id"));
				dto.setJunleid(rs.getInt("junle_id"));
				dto.setArtistid(rs.getInt("artist_id"));
				dto.setPrice(rs.getInt("price"));
				dto.setArrivalid(rs.getInt("arrival_id"));
				dto.setOriginnum(rs.getInt("origin_num"));

				list.add(dto);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
		return list;
	}

	public ArrayList<MerchandiseDTO> searchName(String itemname) throws SQLException {

		ArrayList<MerchandiseDTO> list = new ArrayList<MerchandiseDTO>();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        *");
		sb.append("    FROM");
		sb.append("        merchandise");
		sb.append("    WHERE");
		sb.append("        name LIKE ?");

		try (PreparedStatement ps = this.con.prepareStatement(sb.toString())) {

			ps.setString(1, itemname + "%");
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				MerchandiseDTO dto = new MerchandiseDTO();
				dto.setId(rs.getInt("id"));
				dto.setName(rs.getString("name"));
				dto.setImage(rs.getBinaryStream("image"));
				dto.setCategoryid(rs.getInt("category_id"));
				dto.setJunleid(rs.getInt("junle_id"));
				dto.setArtistid(rs.getInt("artist_id"));
				dto.setPrice(rs.getInt("price"));
				dto.setArrivalid(rs.getInt("arrival_id"));
				dto.setOriginnum(rs.getInt("origin_num"));

				list.add(dto);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
		return list;
	}

	public ArrayList<MerchandiseDTO> cateJunNo(int cateNo, int junNo) throws SQLException {

		ArrayList<MerchandiseDTO> list = new ArrayList<MerchandiseDTO>();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        *");
		sb.append("    FROM");
		sb.append("        merchandise");
		sb.append("    WHERE");
		sb.append("        category_id = ?");
		sb.append("        AND junle_id = ?");

		try (PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
			ps.setInt(1, cateNo);
			ps.setInt(2, junNo);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				MerchandiseDTO dto = new MerchandiseDTO();
				dto.setId(rs.getInt("id"));
				dto.setName(rs.getString("name"));
				dto.setImage(rs.getBinaryStream("image"));
				dto.setCategoryid(rs.getInt("category_id"));
				dto.setJunleid(rs.getInt("junle_id"));
				dto.setArtistid(rs.getInt("artist_id"));
				dto.setPrice(rs.getInt("price"));
				dto.setArrivalid(rs.getInt("arrival_id"));
				dto.setOriginnum(rs.getInt("origin_num"));

				list.add(dto);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
		return list;
	}

	public ArrayList<MerchandiseDTO> cateNo(int cateNo) throws SQLException {

		ArrayList<MerchandiseDTO> list = new ArrayList<MerchandiseDTO>();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        *");
		sb.append("    FROM");
		sb.append("        merchandise");
		if (cateNo != 0) {
			sb.append("    WHERE");
			sb.append("        category_id = ?;");
		}

		try (PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
			if(cateNo != 0){
				ps.setInt(1, cateNo);
			}
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				MerchandiseDTO dto = new MerchandiseDTO();
				dto.setId(rs.getInt("id"));
				dto.setName(rs.getString("name"));
				dto.setImage(rs.getBinaryStream("image"));
				dto.setCategoryid(rs.getInt("category_id"));
				dto.setJunleid(rs.getInt("junle_id"));
				dto.setArtistid(rs.getInt("artist_id"));
				dto.setPrice(rs.getInt("price"));
				dto.setArrivalid(rs.getInt("arrival_id"));
				dto.setOriginnum(rs.getInt("origin_num"));

				list.add(dto);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
		return list;
	}

	public ArrayList<MerchandiseDTO> getMerchandiseBySearchParameter(Integer categoryId, Integer junleId, String name)
			throws SQLException {
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("        *");
		sb.append("    FROM");
		sb.append("        merchandise");
		sb.append("    WHERE");
		sb.append("        '' = ''");

		if (categoryId != null) {
			sb.append("        AND category_id = ?");
		}
		if (junleId != null) {
			sb.append("        AND junle_id = ?");
		}
		if (!(Validate.isEmpty(name))) {
			sb.append("        AND name LIKE ?;");
		}

		try (PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
			int ps_index = 0;
			if (categoryId != null) {
				ps_index++;
				ps.setInt(ps_index, categoryId);
			}
			if (junleId != null) {
				ps_index++;
				ps.setInt(ps_index, junleId);
			}
			if (!(Validate.isEmpty(name))) {
				ps_index++;
				ps.setString(ps_index, '%' + name + '%');
			}
			ResultSet rs = ps.executeQuery();
			ArrayList<MerchandiseDTO> list = new ArrayList<>();
			while (rs.next()) {
				MerchandiseDTO dto = new MerchandiseDTO();
				dto.setId(rs.getInt("id"));
				dto.setName(rs.getString("name"));
				dto.setImage(rs.getBinaryStream("image"));
				dto.setCategoryid(rs.getInt("category_id"));
				dto.setJunleid(rs.getInt("junle_id"));
				dto.setArtistid(rs.getInt("artist_id"));
				dto.setPrice(rs.getInt("price"));
				dto.setArrivalid(rs.getInt("arrival_id"));
				dto.setOriginnum(rs.getInt("origin_num"));
				list.add(dto);
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
	}

	public int updateMerchandise(MerchandiseDTO dto) throws SQLException {
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE");
		sb.append("        merchandise");
		sb.append("    SET");
		sb.append("        name = ?");
		sb.append("        ,image = ?");
		sb.append("        ,category_id = ?");
		sb.append("        ,junle_id = ?");
		sb.append("        ,artist_id = ?");
		sb.append("        ,price = ?");
		sb.append("        ,arrival_id = ?");
		sb.append("        ,origin_num = ?");
		sb.append("    WHERE");
		sb.append("        id = ?");

		try (PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
			ps.setString(1, dto.getName());
			ps.setBlob(2, dto.getImage());
			ps.setInt(3, dto.getCategoryid());
			ps.setInt(4, dto.getJunleid());
			ps.setInt(5, dto.getArtistid());
			ps.setInt(6, dto.getPrice());
			ps.setInt(7, dto.getArrivalid());
			ps.setInt(8, dto.getOriginnum());
			ps.setInt(9, dto.getId());
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
	}

}
