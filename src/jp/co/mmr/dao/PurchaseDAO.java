package jp.co.mmr.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.co.mmr.dto.PurchaseDTO;

/**
 * �w������DAO�N���X
 * @author RyosukeOmori
 *
 */
public class PurchaseDAO {

    private Connection con;

    public PurchaseDAO(Connection con) {
        this.con = con;
    }

    public ArrayList<PurchaseDTO> getPurchaseListByRentalId(int rentalId) throws SQLException {
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT");
        sb.append("        *");
        sb.append("    FROM");
        sb.append("        purchase_detail");
        sb.append("    WHERE");
        sb.append("        rental_id = ?");

        ArrayList<PurchaseDTO> list = new ArrayList<>();
        try(PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
            ps.setInt(1, rentalId);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                PurchaseDTO purchaseDto = new PurchaseDTO();
                purchaseDto.setId(rs.getInt("ID"));
                purchaseDto.setRentalId(rs.getInt("RENTAL_ID"));
                purchaseDto.setMerchandiseId(rs.getInt("MERCHANDISE_ID"));
                purchaseDto.setStatusId(rs.getInt("STATUS_ID"));
                purchaseDto.setOriginId(rs.getInt("ORIGIN_NUM"));
                list.add(purchaseDto);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
    }

    public int setPurchase(PurchaseDTO dto)throws SQLException{

    	StringBuffer sb = new StringBuffer();
    	sb.append("INSERT");
    	sb.append("    INTO");
    	sb.append("        purchase_detail(");
    	sb.append("            rental_id");
    	sb.append("            ,merchandise_id");
    	sb.append("            ,status_id");
    	sb.append("        )");
    	sb.append("    VALUES");
    	sb.append("        (");
    	sb.append("            ?");
    	sb.append("            ,?");
    	sb.append("            ,?");
    	sb.append("        )");

    	try(PreparedStatement ps = this.con.prepareStatement(sb.toString())){
    		ps.setInt(1, dto.getRentalId());
    		ps.setInt(2,dto.getMerchandiseId());
    		ps.setInt(3, dto.getStatusId());

    		int rs = ps.executeUpdate();
    		return rs;
    	}catch (SQLException e) {
			e.printStackTrace();
		}
    	return -1;

    }
}
