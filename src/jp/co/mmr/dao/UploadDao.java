package jp.co.mmr.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import jp.co.mmr.dto.UploadDto;

public class UploadDao {

	private Connection conn;
	public UploadDao(Connection conn){
		this.conn = conn;
	}

	public int upload(UploadDto dto) {

		StringBuffer sb = new StringBuffer();
		sb.append(" UPDATE");
		sb.append("        merchandise");
		sb.append("    SET");
		sb.append("        image = ?");
		sb.append("  WHERE");
		sb.append("        item_id = ?");



			try(PreparedStatement ps = conn.prepareStatement(sb.toString())){
			ps.setString(1, dto.getName());
			ps.setBinaryStream(2, dto.getImage());
			ps.setInt(3, dto.getId());

			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

}
