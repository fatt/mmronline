package jp.co.mmr.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.co.mmr.dto.RentalDTO;

/**
 * ��������DAO�N���X
 * @author RyosukeOmori
 *
 */
public class RentalDAO {

    private Connection con;

    public RentalDAO(Connection con) {
        this.con = con;
    }

    public ArrayList<RentalDTO> getRentalListByUserId(int userId) throws SQLException {
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT");
        sb.append("        *");
        sb.append("    FROM");
        sb.append("        rental_detail");
        sb.append("    WHERE");
        sb.append("        user_id = ?");

        ArrayList<RentalDTO> list = new ArrayList<>();
        try(PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                RentalDTO rentalDto = new RentalDTO();
                rentalDto.setId(rs.getInt("ID"));
                rentalDto.setUserId(rs.getInt("USER_ID"));
                rentalDto.setRecipetDate(rs.getTimestamp("RECEIPT_DATE"));
                rentalDto.setDeliveryDate(rs.getDate("DELIVERY_DATE"));
                rentalDto.setDeliveryTime(rs.getInt("DELIVERY_TIME"));
                rentalDto.setReturnDeadline(rs.getDate("RETURN_DEADLINE"));
                rentalDto.setReturnDate(rs.getDate("RETURN_DATE"));
                rentalDto.setOrderQuantity(rs.getInt("ORDER_QUANTITY"));
                rentalDto.setLendChargeId(rs.getString("LEND_CHARGE_ID"));
                rentalDto.setReturnChargeId(rs.getString("RETURN_CHARGE_ID"));
                rentalDto.setStatusId(rs.getInt("STATUS_ID"));
                list.add(rentalDto);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
    }

    public ArrayList<RentalDTO> getRentalListByUserIdWithReceiptSort(int userId, boolean isAsc) throws SQLException {
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT");
        sb.append("        *");
        sb.append("    FROM");
        sb.append("        rental_detail");
        sb.append("    WHERE");
        sb.append("        user_id = ?");
        sb.append("    ORDER BY");
        sb.append("        receipt_date " + (isAsc?"ASC":"DESC"));

        ArrayList<RentalDTO> list = new ArrayList<>();
        try(PreparedStatement ps = this.con.prepareStatement(sb.toString())) {
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                RentalDTO rentalDto = new RentalDTO();
                rentalDto.setId(rs.getInt("ID"));
                rentalDto.setUserId(rs.getInt("USER_ID"));
                rentalDto.setRecipetDate(rs.getTimestamp("RECEIPT_DATE"));
                rentalDto.setDeliveryDate(rs.getDate("DELIVERY_DATE"));
                rentalDto.setDeliveryTime(rs.getInt("DELIVERY_TIME"));
                rentalDto.setReturnDeadline(rs.getDate("RETURN_DEADLINE"));
                rentalDto.setReturnDate(rs.getDate("RETURN_DATE"));
                rentalDto.setOrderQuantity(rs.getInt("ORDER_QUANTITY"));
                rentalDto.setLendChargeId(rs.getString("LEND_CHARGE_ID"));
                rentalDto.setReturnChargeId(rs.getString("RETURN_CHARGE_ID"));
                rentalDto.setStatusId(rs.getInt("STATUS_ID"));
                list.add(rentalDto);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
    }

    public int setRental(RentalDTO dto)throws SQLException{

    	StringBuffer sb = new StringBuffer();
    	sb.append("INSERT");
    	sb.append("    INTO");
    	sb.append("        rental_detail(");
    	sb.append("            user_id");
    	sb.append("            ,receipt_date");
    	sb.append("            ,delivery_date");
    	sb.append("            ,delivery_time");
    	sb.append("            ,return_deadline");
    	sb.append("            ,order_quantity");
    	sb.append("            ,status_id");
    	sb.append("        )");
    	sb.append("    VALUES");
    	sb.append("        (");
    	sb.append("            ?");
    	sb.append("            ,?");
    	sb.append("            ,?");
    	sb.append("            ,?");
    	sb.append("            ,?");
    	sb.append("            ,?");
    	sb.append("            ,?");
    	sb.append("        )");

    	try(PreparedStatement ps = this.con.prepareStatement(sb.toString())){

    		ps.setInt(1, dto.getUserId());
    		ps.setTimestamp(2,dto.getRecipetDate());
    		ps.setDate(3, dto.getDeliveryDate());
    		ps.setInt(4, dto.getDeliveryTime());
    		ps.setDate(5, dto.getReturnDeadline());
    		ps.setInt(6, dto.getOrderQuantity());
    		ps.setInt(7, dto.getStatusId());

    		int rs = ps.executeUpdate();

    		return rs;

    	}catch(SQLException e){
    		e.printStackTrace();
    	}
    	return -1;

    }

    public int searchId(RentalDTO dto){

    	StringBuffer sb = new StringBuffer();
    	sb.append("SELECT");
    	sb.append("        id");
    	sb.append("    FROM");
    	sb.append("        rental_detail");
    	sb.append("    ORDER BY");
    	sb.append("        receipt_date DESC limit ?");


    	int orderId = 0;
    	try(PreparedStatement ps = this.con.prepareStatement(sb.toString())){
    		ps.setInt(1, 1);
    		ResultSet rs = ps.executeQuery();
    		if (rs.next()) {
        		orderId = rs.getInt("id");
			}


    	}catch(SQLException e){
    		e.printStackTrace();
    	}
    	return orderId;
    }


}
