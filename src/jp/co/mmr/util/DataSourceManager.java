package jp.co.mmr.util;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DataSourceManager {

	public static Connection getConnection()
	throws NamingException, SQLException{
		try {
			Connection conn = null;
			Context ctx = new InitialContext();
			DataSource ds = (DataSource)ctx.lookup("java:comp/env/jdbc/mysql");
			conn = ds.getConnection();
			return conn;

		} catch (NamingException  e) {
			throw e;
		} catch (SQLException e) {
			throw e;
		}
	}
}
