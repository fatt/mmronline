package jp.co.mmr.util;

import java.util.regex.Pattern;

/**
 * ���̓`�F�b�N�p�N���X
 * @author RyosukeOmori
 */
public abstract class Validate {

    /**
     * �^����ꂽString�l���󖔂�null�̏ꍇ��true��Ԃ�
     * @param str
     * @return
     */
    public static boolean isEmpty(String str) {
        if (str == null || str.length() == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * �^����ꂽString�l�����l�݂̂̏ꍇ��true��Ԃ�
     * @param str
     * @return
     */
    public static boolean isNumber(String str) {
        return Pattern.compile("\\d+").matcher(str).matches();
    }

    /**
     * �^����ꂽString�l��1byte����(���p�p����)�݂̂̏ꍇ��true��Ԃ�
     * @param str
     * @return
     */
    public static boolean isHankaku(String str) {
        return Pattern.compile("^[0-9a-zA-Z]+$").matcher(str).matches();
    }

    /**
     * isHankaku + �^����ꂽint�l�ȏ�̕������ł���ꍇ��true��Ԃ�
     * @param str
     * @param len
     * @return
     */
    public boolean isHankakuWithLength(String str, int len) {
        return (Validate.isHankaku(str) && str.length() >= len);
    }

    /**
     * ���̓`�F�b�N���s��Integer.parseInt()
     * @param str
     * @return
     */
    public static Integer convertStrToInteger(String str) {
        if (!(Validate.isEmpty(str)) && Validate.isNumber(str)) {
            return Integer.parseInt(str);
        } else {
            return null;
        }
    }
    
    public static String getStrByObject(Object value) {
        if (value != null) {
            String str = String.valueOf(value);
            if (!(Validate.isEmpty(str))) {
                return str;
            }
        }
        return null;
    }

    /**
     * 文字列の置換を行う
     *
     * @param input 処理の対象の文字列
     * @param pattern 置換前の文字列
     * @param replacement 置換後の文字列
     * @return 置換処理後の文字列
     */
    static public String substitute(String input, String pattern, String replacement) {
        // 置換対象文字列が存在する場所を取得
        int index = input.indexOf(pattern);
        // 置換対象文字列が存在しなければ終了
        if(index == -1) {
            return input;
        }
        // 処理を行うための StringBuffer
        StringBuffer buffer = new StringBuffer();
        buffer.append(input.substring(0, index) + replacement);
        if(index + pattern.length() < input.length()) {
            // 残りの文字列を再帰的に置換
            String rest = input.substring(index + pattern.length(), input.length());
            buffer.append(substitute(rest, pattern, replacement));
        }
        return buffer.toString();
    }


    /**
	 * HTML 出力用に次の置換を行う
	 * & -> &
	 * < -> <
	 * > -> >
	 * " -> "
	 * % -> %
	 * @param input 置換対象の文字列
	 * @return 置換処理後の文字列
     * */
    static public String HTMLEscape(String input) {
        input = substitute(input, "&",  "&");
        input = substitute(input, "<",  "<");
        input = substitute(input, ">",  ">");
        input = substitute(input, "\"", "\"");
        input = substitute(input, "%", "%");
        return input;
    }

    /**
     * SQL文出力用に次の置換を行う
     * ' -> ''
     * \ -> \\
     *
     * @param input 置換対象の文字列
     * @return 置換処理後の文字列
     */
    static public String SQLEscape(String input) {
    	input = substitute(input, "'", "''");
    	input = substitute(input, "\\", "\\\\");
    	return input;
    }
}
