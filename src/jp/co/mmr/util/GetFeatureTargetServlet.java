package jp.co.mmr.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.mmr.dao.MerchandiseDAO;
import jp.co.mmr.dto.MerchandiseDTO;

/**
 * Servlet implementation class SearchServlet
 */
//@WebServlet(name = "UtilSearchServlet", urlPatterns = { "/utilSearchServlet" })
@WebServlet("/getFeatureTarget")
public class GetFeatureTargetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		    resp.setContentType("application/json; charset=utf-8");

		    Integer featureId = Validate.convertStrToInteger(Validate.getStrByObject(req.getParameter("featureId")));

		    PrintWriter pw = resp.getWriter();
		    try(Connection con = DataSourceManager.getConnection()) {
		    	MerchandiseDAO merchandiseDao = new MerchandiseDAO(con);
		        ArrayList<MerchandiseDTO> merchandiseList = merchandiseDao.getMerchandiseByFeatureId(featureId);
		        pw.println("{\"merchandiseList\" : [");
		        for(int i = 0; i < merchandiseList.size(); i++) {
	                pw.print("{");
	                pw.print("\"line\" : \"" + (i+1) + "\",");
	                pw.print("\"id\" : \"" + merchandiseList.get(i).getId() + "\",");
	                pw.print("\"name\" : \"" + merchandiseList.get(i).getName() + "\",");
	                pw.print("\"arrival\" : \"" + Converter.getArrivalByArrivalId(merchandiseList.get(i).getArrivalid()) + "\",");
	                pw.print("\"category\" : \"" + Converter.getCategoryByCategoryId(merchandiseList.get(i).getCategoryid()) + "\",");
	                pw.print("\"junle\" : \"" + Converter.getJunleByJunleId(merchandiseList.get(i).getJunleid()) + "\",");
	                pw.print("\"artist\" : \"" + Converter.getArtistByArtistId(merchandiseList.get(i).getArtistid()) + "\",");
	                pw.print("\"price\" : \"" + merchandiseList.get(i).getPrice() + "\"");
	                pw.print("}");
		            if (i != merchandiseList.size() - 1) {
						pw.print(",");
					}
		        }
		        pw.println("],");
		        pw.print("\"listSize\" : \"" + merchandiseList.size() + "\"");
		        pw.println("}");
		    } catch (SQLException | NamingException e) {
	            e.printStackTrace();
	            resp.sendRedirect("error.jsp");
	        }
	}


}
