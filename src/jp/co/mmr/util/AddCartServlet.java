package jp.co.mmr.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.mmr.dao.MerchandiseDAO;
import jp.co.mmr.dto.MerchandiseDTO;
import jp.co.mmr.util.DataSourceManager;
import jp.co.mmr.util.Validate;

@WebServlet("/addCart")
@SuppressWarnings("unchecked")
public class AddCartServlet extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {


		HttpSession session = req.getSession();
		String id = req.getParameter("delete");

		Integer deleteid = Validate.convertStrToInteger(id);

		try (Connection con = DataSourceManager.getConnection()) {
		MerchandiseDAO dao = new MerchandiseDAO(con);
		MerchandiseDTO dto = dao.getMerchandiseById(deleteid);

		ArrayList<MerchandiseDTO> list = (ArrayList<MerchandiseDTO>) session.getAttribute("cartItem");
		int listno = list.indexOf(dto);

		list.remove(listno);
		}catch(SQLException | NamingException | ArrayIndexOutOfBoundsException e){
			e.printStackTrace();
		}
		req.getRequestDispatcher("/WEB-INF/jsp/cart.jsp").forward(req, res);
	}


	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		HttpSession session = req.getSession();
		req.setCharacterEncoding("UTF-8");
		String id = req.getParameter("itemid");
		Integer itemid = Validate.convertStrToInteger(id);
		System.out.println(id);

		try (Connection con = DataSourceManager.getConnection()) {
			MerchandiseDAO dao = new MerchandiseDAO(con);
			MerchandiseDTO dto = dao.getMerchandiseById(itemid);
			int count = 0;

			try {
				if (session.getAttribute("cartItem") == null) {
					ArrayList<MerchandiseDTO> list = new ArrayList<MerchandiseDTO>();
					list.add(dto);
					session.setAttribute("cartItem", list);
					count = list.size();
					int price = dto.getPrice();
					session.setAttribute("price", price);
				} else {
					ArrayList<MerchandiseDTO> list = (ArrayList<MerchandiseDTO>) session.getAttribute("cartItem");
					list.add(dto);
					count = list.size();
					int sum = 0;
					for (int i = 0; i < list.size(); i++) {
						dto = list.get(i);
						sum += dto.getPrice();
					}
					session.setAttribute("price", sum);

				}

				session.setAttribute("count", count);
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		}

		req.getRequestDispatcher("/WEB-INF/jsp/cart.jsp").forward(req, res);

	}

}
