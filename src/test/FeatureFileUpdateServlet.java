package test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import jp.co.mmr.dao.FeatureDAO;
import jp.co.mmr.dto.FeatureDTO;
import jp.co.mmr.util.DataSourceManager;
import jp.co.mmr.util.Validate;

@WebServlet("/featureFileUpdate")
public class FeatureFileUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		try (Connection con = DataSourceManager.getConnection()) {
    		FeatureDAO dao = new FeatureDAO(con);
    		int id = Validate.convertStrToInteger(request.getParameter("updateId"));
    		FeatureDTO dto = dao.getFeatureById(id);
    		for(Part p :request.getParts()){
    			if ("updateImage".equals(p.getName())) {
    				dto.setImage(p.getInputStream());
    			}
    		}
    		
    		if (1 == dao.updateFeature(dto)) {
    			response.sendRedirect("FileUpload.jsp");
            } else {
                System.out.println("error!!");
            }
    	}catch(NumberFormatException | SQLException | NamingException e){
    		e.printStackTrace();
    	}
	}
}
