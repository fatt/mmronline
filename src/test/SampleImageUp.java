package test;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.naming.NamingException;

import jp.co.mmr.util.DataSourceManager;

public class SampleImageUp {

	public static void main(String[] args) throws SQLException{
		try (Connection con = DataSourceManager.getConnection()) {

			 // 画像ファイルを読み込んでバイト配列取得
			FileInputStream fis = new FileInputStream("");
           FileChannel channel = fis.getChannel();
           ByteBuffer buffer = ByteBuffer.allocate((int) channel.size());
           channel.read(buffer);
           buffer.clear();
           byte[] bytes = new byte[buffer.capacity()];
           buffer.get(bytes);
           channel.close();
           fis.close();

           // 登録
           StringBuffer sb = new StringBuffer();
           sb.append("INSERT");
           sb.append("    INTO");
           sb.append("        MERCHANDISE(image)");
           sb.append("    VALUES");
           sb.append("        (?);");

           try(PreparedStatement ps = con.prepareStatement(sb.toString())){
           	ps.setBytes(1, bytes);
           	ps.execute();
           }

		}catch(SQLException | NamingException | IOException e){
			e.printStackTrace();
		}
	}

}
