package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import jp.co.mmr.dao.FeatureDAO;
import jp.co.mmr.dto.FeatureDTO;
import jp.co.mmr.util.DataSourceManager;

@WebServlet("/featureFileUpload")
public class FeatureFileUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		try (Connection con = DataSourceManager.getConnection()) {
    		FeatureDTO dto = new FeatureDTO();
    		for(Part p :request.getParts()){
    			if ("image".equals(p.getName())) {
    				dto.setImage(p.getInputStream());
    			} else if("title".equals(p.getName())) {
    				BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream(), "UTF-8"));
    				dto.setTitle(br.readLine());
    			} else if("period".equals(p.getName())) {
    				BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
    				dto.setPeriod(Integer.parseInt(br.readLine()));
    			}
    		}
    		
    		FeatureDAO dao = new FeatureDAO(con);
    		if (1 == dao.setFeature(dto)) {
    		    System.out.println("success!!");
            } else {
                System.out.println("error!!");
            }
    	}catch(NumberFormatException | SQLException | NamingException e){
    		e.printStackTrace();
    	}
	}
}
