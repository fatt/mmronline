<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>MMR -ホーム-</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="slick/slick.css" rel="stylesheet" type="text/css">
<link href="slick/slick-theme.css" rel="stylesheet" type="text/css">
<link href="css/form_custom.css" rel="stylesheet" type="text/css">
<link href="css/header.css" rel="stylesheet" type="text/css">
<link href="css/footer.css" rel="stylesheet" type="text/css">
<link href="css/home.css" rel="stylesheet" type="text/css">
</head>
<body>
	<jsp:include page="homeheader.jsp" flush="true"/>

	<c:choose>
		<c:when test="${requestScope.loginSucMsg != null}">
			<div class="alert alert-success"><c:out value="${requestScope.loginSucMsg}" /></div>
		</c:when>
		<c:when test="${sessionScope.loginErrMsg != null}">
			<div class="alert alert-danger"><c:out value="${sessionScope.loginErrMsg}" /></div>
		</c:when>
		<c:when test="${requestScope.addUser_message != null}">
			<div class="alert alert-danger"><c:out value="${requestScope.addUser_message}" /></div>
		</c:when>
	</c:choose>

	<div id="body">
		<div class="container">
			<div id="features">
				<c:forEach var="feature" items="${requestScope.features}"
					varStatus="loop">
					<div class="caption col-md-8 col-md-offset-2">
						<a href="searchMerchandise?feature_id=${feature.id}"><img class="feature-img"
							src="getImage?feature_id=${feature.id}" /></a>
						<p class="caption-p">
							<c:out value="${feature.title}" />
						</p>
					</div>
				</c:forEach>
			</div>
		</div>

		<c:if test="${sessionScope.loginUser != null}">
			<div id="histories">
				<div class="col-md-offset-1"></div>
				<div class="multiple-items slider">
					<c:forEach var="merchandise"
						items="${requestScope.merchandise_history}">
						<div><img src="getImage?merchandise_id=${merchandise.id}" /></div>
					</c:forEach>
				</div>
			</div>
		</c:if>

	</div>

	<jsp:include page="footer.jsp" flush="true"/>

	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="slick/slick.js"></script>
	<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/form_custom.js"></script>
	<script type="text/javascript" src="js/header.js"></script>
	<script type="text/javascript" src="js/home.js"></script>

</body>
</html>