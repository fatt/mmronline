<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>カート内画面</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="css/form_custom.css" rel="stylesheet" type="text/css">
<link href="css/header.css" rel="stylesheet" type="text/css">
<link href="css/footer.css" rel="stylesheet" type="text/css">
<style type="text/css">
ul {
	font-size: 0;
}

ul li {
	text-align: center;
	display: inline-block; /
	display: inline; /
	zoom: 1;
	font-size: 16px;
	width: 70px;
	vertical-align: top
}

.container {
	max-width: none !important;
	width: 970px;
}

.navbar-collapse {
	display: block !important;
	height: auto !important;
	padding-bottom: 0;
	overflow: visible !important;
}

.navbar-toggle {
	display: none;
}

.navbar-brand {
	margin-left: -15px;
}
</style>
</head>
<body>
	<jsp:include page="homeheader.jsp" flush="true" />

	<div class="container">
		<div class="col-md-12">
			<h1 style="text-align: center">カート内確認画面</h1>
			<div class="row">
				<div class="col-xs-12">
					<table class="table">
						<tr>
							<th></th>
							<th style="text-align: center">タイトル</th>
							<th style="text-align: center">新旧</th>
							<th style="text-align: center">カテゴリ</th>
							<th style="text-align: center">ジャンル</th>
							<th style="text-align: center">アーティスト</th>
							<th style="text-align: center">値段</th>
							<th style="text-align: center">削除</th>
						</tr>

						<c:forEach var="cart" items="${sessionScope.cartItem }"
							varStatus="loop">
							<tr>
								<td style="text-align: center"><img style="width: 100px;"
									src="getImage?merchandise_id=${cart.id}" /></td>
								<td style="text-align: center"><c:out value="${cart. name }" /></td>
								<td style="text-align: center"><c:out value="${cart. arrivalid }" /></td>
								<td style="text-align: center"><c:out value="${cart. categoryid }" /></td>
								<td style="text-align: center"><c:out value="${cart. junleid }" /></td>
								<td style="text-align: center"><c:out value="${cart. artistid }" /></td>
								<td style="text-align: center"><c:out value="${cart. price }" /></td>
								<form action="incart" method="post">
									<td style="text-align: center"><a href="incart"><button class="btn btn-default"
												type="submit">削除</button> <input type="hidden" name="delete"
											value="${loop.index }"></a></td>
								</form>
							</tr>
						</c:forEach>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-11">
			<h4 style="float: right;">
				数量
				<c:out value="${count}" />
				件
			</h4>
		</div>
		<div class="col-md-11">
			<h4 style="float: right;">
				合計金額
				<c:out value="${price}" />
				円
			</h4>
		</div>
		<div class="col-md-12">
			<h2 style="text-align: center">
				<ul>
					<li><a href="home"><button class="btn btn-default"
								type="button">ホーム</button></a></li>

					<li><form action="preorderschedule" method="post">
							<a href="preorderschedule"><button class="btn btn-default"
									type="submit">レンタル</button></a>
						</form></li>
				</ul>
			</h2>
		</div>
	</div>
	<jsp:include page="footer.jsp" flush="true" />
	<script type="text/javascript"
			src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/cart.js" type="text/javascript"></script>
</body>
</html>