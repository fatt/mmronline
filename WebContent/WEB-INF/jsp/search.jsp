<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>商品検索結果</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="css/form_custom.css" rel="stylesheet" type="text/css">
<link href="css/header.css" rel="stylesheet" type="text/css">
<link href="css/footer.css" rel="stylesheet" type="text/css">
<style type="text/css">
span {
	display: inline-block;
	width: 30%;
	text-align: center;
}
td.td-img, img.item-img {
	width: 100px;
}
-->
</style>

</head>
<body>
	<jsp:include page="header.jsp" flush="true" />

	<input type="hidden" name="homeFeature" value="${requestScope.featureId}">
	<input type="hidden" name="fromHome" value="${requestScope.fromHome}">
	<input type="hidden" name="homeCategory" value="${requestScope.categoryNo}">
	<input type="hidden" name="homeJunle" value="${requestScope.junleNo}">
	<input type="hidden" name="homeName" value="${requestScope.itemName}">

	<div class="container" style="margin-top: 20px">
		<div class="col-md-8 col-md-offset-2">
			<p id="text-size"></p>
			<p id="text-result"></p>
		</div>

		<table id="table-result" class="table">
			<tr>
				<th></th>
				<th>画像</th>
				<th>タイトル</th>
				<th>新旧</th>
				<th>カテゴリ</th>
				<th>ジャンル</th>
				<th>アーティスト</th>
				<th>値段</th>
				<th>カート</th>
			</tr>
		</table>
	</div>
	<span id="prev"><c:out value="←" /></span>　
	<span id="page"></span>　
	<span id="next"><c:out value="→" /></span>
</body>
	<jsp:include page="footer.jsp" flush="true"/>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/itemSearch.js" type="text/javascript"></script>

</html>