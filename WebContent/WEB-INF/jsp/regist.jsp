<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@
	page import="jp.co.mmr.dto.UserAddDTO, java.util.ArrayList, java.util.Date"
 %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>新規会員登録</title>
	<style type="text/css">
	<!--
		.center{ text-align:center; }
		table{ margin-left:auto;
			   margin-right:auto; }
	-->
	</style>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/footer.css" rel="stylesheet" type="text/css">
</head>
	<div id="header_left">
		<a href="index.jsp"><img class="header_img" src="img/MMR-logo-2.png" /></a>
	</div><hr>
<body>
	<h1 class="center">新規会員登録</h1><br>
	<h2 class="center">お客様情報</h2>
	<form method="post" action="account">
    	<table border=1>
         <tr>
            <td class="center">
              	メールアドレス<br>
            	確認用
            </td>
            <td>
                <input type="email" name="mailAddress" maxlength="50" required value='<c:out value="${add_user.mailAddress}"/>'><br>
                <input type="email" name="forCheck" maxlength="50" required value='<c:out value="${add_user.checkMailAddress}"/>'>
                <% if (null != request.getAttribute("mail_error_message")) { %>
					<div style="color:red"><%= request.getAttribute ("mail_error_message") %> </div>
				<% } %>
            </td>
         </tr>
         <tr>
            <td class="center">
            	お名前<br>
            	フリガナ
            </td>
            <td>
                <input type="text" name="name_1" maxlength="20" required value='<c:out value="${add_user.userName}"/>'><br>
                <input type="text" name="name_2" maxlength="20" required value='<c:out value="${add_user.userHurigana}"/>'>
                <% if (null != request.getAttribute("name_error_message")) { %>
					<div style="color:red"><%= request.getAttribute ("name_error_message") %> </div>
				<% } %>
            </td>
         </tr>
         <tr>
            <td class="center">
            	パスワード<br>
            	確認用

            </td>
            <td>
                 <input type="password" name="password" maxlength="20" minlength="8" required /><br>
                 <input type="password" name="passwordForCheck" minlength="8" maxlength="20" required /><br>
				<% if (null != request.getAttribute("password_error_message")) { %>
				<div style="color:red"><%= request.getAttribute ("password_error_message") %> </div>
				<% } %>
                 	※半角英数8文字以上20文字以下<br>
            </td>
         </tr>
         <tr>
             <td class="center">
				住所
			</td>
            <td>
            	郵便番号 〒
                <input type="number" name="postNum_1" maxlength="3" required size="3" value='<c:out value="${add_user.post1}"/>'>
				ー
                <input type="number" name="postNum_2" maxlength="4" required size="4" value='<c:out value="${add_user.post2}"/>'><br>
                	※都道府県名から番地までを入力してください。<br>
                	住所１
                <input type="text" name="address_1" maxlength="50" required size="50" value='<c:out value="${add_user.address1}"/>'><br>
                	※建物名から部屋番号までを入力してください。<br>
                	住所２
                <input type="text" name="address_2" maxlength="50" required size="50" value='<c:out value="${add_user.address2}"/>'>
            </td>
         </tr>
         <tr>
            <td class="center">
            	電話番号
            </td>
            <td>
                <input type="number" name="telNum" maxlength="11" required value='<c:out value="${add_user.tell}"/>'>
				<% if (null != request.getAttribute("tell_error_message")) { %>
					<div style="color:red"><%= request.getAttribute ("tell_error_message") %> </div>
				<% } %>
            </td>
         </tr>
         <tr>
            <td class="center">
            	生年月日</td>
            <td>
                <input type="number" name="brithYear" maxlength="4" required size="4">
                	年
                <input type="number" name="brithMonth" maxlength="2" required size="4">
                	月
                <input type="number" name="brithDay" maxlength="2" required size="4">
             	  	日<br/>
                	※西暦で入力してください
				<% if (null != request.getAttribute("birthday_error_message")) { %>
					<div style="color:red"><%= request.getAttribute ("birthday_error_message") %> </div>
				<% } %>
            </td>
         </tr>
         <tr>
            <td class="center">
            	カード名義人<br/>
            	カード番号<br/>
            	有効期限<br/>
            	セキュリティコード
            </td>
        	<td>
	            <input type="text" name="ownerName" maxlength="50" required  value='<c:out value="${add_user.creditOwnerName}"/>'><br>

	            <input type="number" name="cardNum_1" maxlength="4" minlength="4" required size="4">
	            <input type="number" name="cardNum_2" maxlength="4" minlength="4" required size="4">
	            <input type="number" name="cardNum_3" maxlength="4" minlength="4" required size="4">
	            <input type="number" name="cardNum_4" maxlength="4" minlength="4" required size="4">
	            <br>
	            <input type="number" name="validatePeriod" maxlength="6" required size="6"  value='<c:out value="${add_user.expirationDate}"/>'><br/>
	            <input type="number" name="securityCode" maxlength="4" minlength="3" required size="4"><br/>
				<% if (null != request.getAttribute("card_error_message")) { %>
					<div style="color:red"><%= request.getAttribute ("card_error_message") %> </div>
				<% } %>
         	</td>
         </tr>
    	 </table>
	 	<br>
	 	<c:out value="${ requestScope.error_message }" /><br>
	 	<div class="center"><input type="submit" value="登録"></div>
	</form>
	<br><br>
	<div>

	</div>
</body>
</html>