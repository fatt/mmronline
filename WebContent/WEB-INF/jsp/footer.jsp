<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
		<style type="text/css">
	    <!--
	    	h6 { text-align: right; }
	    -->
    	</style>
<body>
<div id="footer">
	<div id="bread-crumbs">
		<a href="index.jsp">ホームへ</a>
	</div>
	<div id="copy_right">
	</div>
</div>
<h6><c:out value="copyright © 2017 music & movie rental Co., Ltd All rights reserved." /></h6>
</body>
</html>