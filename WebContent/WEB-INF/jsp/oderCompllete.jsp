<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>注文受付完了</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="css/form_custom.css" rel="stylesheet" type="text/css">
<link href="css/header.css" rel="stylesheet" type="text/css">
<link href="css/footer.css" rel="stylesheet" type="text/css">

</head>
<body>
	<jsp:include page="homeheader.jsp" flush="true" />
	<div class="container" style="margin-top: 20px">
		<h2 style="text-align: center">注文受付完了</h2>
		<div style="text-align: center">
		注文番号:
		<c:out value="${rentalSchedule.id }" />

		<br>
		<c:out value="${rentalSchedule.orderQuantity}" />
		点のご注文を承りました。 <br>
		<c:out value="${rentalSchedule.deliveryDate }" />
		,
		<c:choose>
			<c:when test="${rentalSchedule.deliveryTime == 1}">午前中</c:when>
			<c:when test="${rentalSchedule.deliveryTime == 2}">12:00~15:00</c:when>
			<c:when test="${rentalSchedule.deliveryTime == 3}">15:00~18:00</c:when>
			<c:otherwise>18:00~21:00</c:otherwise>
		</c:choose>
		にお届けする予定です。 <br> 返却予定日は
		<c:out value="${rentalSchedule.returnDeadline }" />
		です。 <br> ご注文ありがとうございます。
</div>

		<div class="col-md-12">
			<h2 style="text-align: center">
				<a href="logout"><button class="btn btn-default" type="button">ログアウト</button></a>

				<a href="home"><button class="btn btn-default" type="submit">ホームへ</button></a>
			</h2>
		</div>
	</div>

	<%
		session.removeAttribute("cartItem");
	%>
	<%
		session.removeAttribute("price");
	%>
	<%
		session.removeAttribute("count");
	%>

	<jsp:include page="footer.jsp" flush="true" />
	<script type="text/javascript"
		src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/cart.js" type="text/javascript"></script>
</body>
</html>