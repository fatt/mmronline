<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
<div id="header">
	<!-- ロゴ -->
	<div id="header_left">
		<a href="index.jsp"><img class="header_img" src="img/MMR-logo-2.png" /></a>
	</div>
	<div id="header_center">
		<!-- アクション：アカウント -->
		<div id="header_above">
			<c:choose>
				<c:when test="${sessionScope.loginUser != null}">
					<a class="above_element" href="logout"><button type="button">ログアウト</button></a>
				</c:when>
				<c:otherwise>
					<a class="above_element" href="regist"><button type="button">会員登録</button></a>
					<form action="login" method="post">
						<button class="header_form custom-submit" type="submit">ログイン</button>
						<input class="header_form custom-text" type="password" name="login-password" placeholder="パスワード" />
						<input class="header_form custom-text" type="text" name="login-id" placeholder="メールアドレス" />
					</form>
				</c:otherwise>
			</c:choose>
		</div>
		<!-- 入力フォーム -->
		<div id="header_below">
			<div class="col-md-10 col-md-offset-2">
				<form class="form-inline" action="searchMerchandise" method="get">
					<div class="form-group">
						<select class="form-control" name="searchCategory">
							<option value="" disabled selected>カテゴリを選択</option>
							<option value="">すべて</option>
							<option value="1">CD</option>
							<option value="2">DVD</option>
							<option value="3">Blu-ray</option>
						</select>
					</div>
					<div class="form-group">
						<select class="form-control" name="searchJunle">
							<option value="" disabled selected style="display:none;">ジャンルを選択</option>
						</select>
					</div>
					<div class="form-group">
						<input class="form-control" type="text" name="searchName" placeholder="商品名" />
					</div>
					<button id="btn-search" class="btn btn-default">検索</button>
				</form>
			</div>
		</div>
	</div>
	<!-- カート -->
	<div id="header_right">
		<a href="incart"><img class="header_img" src="img/cart_sample.png" /></a>
	</div>
</div>
</body>
</html>