<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>お届け日時確認</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet"
	type="text/css">
<link href="css/form_custom.css" rel="stylesheet" type="text/css">
<link href="css/header.css" rel="stylesheet" type="text/css">
<link href="css/footer.css" rel="stylesheet" type="text/css">
<style type="text/css">
ul {
	font-size: 0;
}

ul li {
	text-align: center;
	display: inline-block; /
	display: inline; /
	zoom: 1;
	font-size: 16px;
	width: 73px;
	vertical-align: top
}
</style>

</head>
<body>
	<jsp:include page="homeheader.jsp" flush="true" />
	<div class="container" style="margin-top: 20px">
		<form action="orderSchedule" method="post">
			<h1 style="text-align: center">お届け日時確認</h1>
			<h2 style="text-align: center">お届け日選択</h2>
			<div style="text-align: center">
			<input type="date" cmanCLDat="USE:ON" style="width: 150px;"
				name="Date" required> </div>
			<h2 style="text-align: center">日時選択</h2>

			<div class="col-md-2 col-md-offset-4" style="text-align: left;">
				<label><input type="radio" name="time" value="1" checked
					required />午前中</label><br> <label><input type="radio"
					name="time" value="2">12:00~15:00</label>
			</div>


			<div class="col-md-5 col-md-offset-1" style="text-align: left">
				<label><input type="radio" name="time" value="3">15:00~18:00</label><br>
				<label><input type="radio" name="time" value="4">18:00~21:00</label>
			</div>


			<div class="col-md-12">
				<table class="table">

					<tr>
						<th>タイトル</th>
						<th>カテゴリ</th>

					</tr>
					<c:forEach var="item" items="${sessionScope.cartItem }">
						<tr>
							<td><c:out value="${item. name }" /></td>
							<td><c:out value="${item. categoryid }" /></td>
						</tr>
					</c:forEach>
					<h4>
						<c:out value="${count}" />
						件をレンタルします。
					</h4>

				</table>
			</div>
			<div class="col-md-12">
				<h2 style="text-align: center">
					<ul>
						<li><a href="incart"><button class="btn btn-default"
									type="button">カートへ</button></a></li>
						<li><a href="orderSchedule"><button
									class="btn btn-default" type="submit">確定</button></a></li>

					</ul>
				</h2>
			</div>
		</form>
	</div>

	<jsp:include page="footer.jsp" flush="true" />
	<script type="text/javascript"
		src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>


	<script src="js/cart.js" type="text/javascript"></script>
	<script src="http://web-designer.cman.jp/freejs/cmanCalendar_v092.js"
		charset="utf-8"></script>

</body>
</html>