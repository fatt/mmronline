<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

</head>
<body>

<form action="featureFileUpload" method="post" enctype="multipart/form-data">
  <p>特集フォーム</p>
  <input type="text" name="title" placeholder="title">
  <input type="text" name="period" placeholder="period">
  <input type="file" name="image">
  <input type="submit" value="送信">
</form>
<form action="featureFileUpdate" method="post" enctype="multipart/form-data">
	<p>特集画像更新</p>
	<input type="text" name="updateId" placeholder="id" />
	<input type="file" name="updateImage">
	<input type="submit" value="送信">
</form>

<form action="merchandiseFileUpload" method="post" enctype="multipart/form-data">
  <p>商品フォーム</p>
  <input type="text" name="name" placeholder="name">
  <input type="file" name="image">
  <input type="text" name="category_id" placeholder="category_id">
  <input type="text" name="junle_id" placeholder="junle_id">
  <input type="text" name="artist_id" placeholder="artist_id">
  <input type="text" name="price" placeholder="price">
  <input type="text" name="arrival_id" placeholder="arrival_id">
  <input type="text" name="origin_num" placeholder="origin_num">
  <input type="submit" value="送信">
</form>
<form action="merchandiseFileUpdate" method="post" enctype="multipart/form-data">
  <p>商品更新フォーム</p>
  <input type="text" name="id" placeholder="id">
  <input type="text" name="name" placeholder="name">
  <input type="file" name="image">
  <input type="text" name="category_id" placeholder="category_id">
  <input type="text" name="junle_id" placeholder="junle_id">
  <input type="text" name="artist_id" placeholder="artist_id">
  <input type="text" name="price" placeholder="price">
  <input type="text" name="arrival_id" placeholder="arrival_id">
  <input type="text" name="origin_num" placeholder="origin_num">
  <input type="submit" value="送信">
</form>

<br><br><br>
<div>
<input type="text" id="img-id" name="feature_id" placeholder="ID" />
<input type="button" id="btn-feature" value="特集画像検索" />
<input type="button" id="btn-merchandise" value="商品画像表示" />
</div>
<div><img id="result" /></div>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript">
$(function() {
	$('#btn-feature').click(function() {
		$('#result').attr('src', 'getImage?feature_id=' + $('#img-id').val());
		/* $.get('getImage', {
			feature_id: $('#feature').val()
		}, function(data) {
			$('#result').attr('src', data);
		}); */
	});
	$('#btn-merchandise').click(function() {
		$('#result').attr('src', 'getImage?merchandise_id=' + $('#img-id').val());
	});
})
</script>
</body>
</html>