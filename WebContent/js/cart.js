$(document).ready(function() {
	// selectタグの自動生成
	$('select[name=searchCategory]').on('change', function() {
		var category_val = $(this).val();
		$('select[name=searchJunle] option').remove();
		$('<option value="" disabled selected>ジャンルを選択</option>').appendTo('select[name=searchJunle]');
		$.ajax({
			url: "getJunle",
			type: "POST",
			dataType: 'json',
			data: {
				categoryId : category_val
			},
			success : function(data) {
				$('<option value="">すべて</option>').appendTo('select[name=searchJunle]');
				$(data.junleList).each(function() {
					$('<option value="' + this.id + '">' + this.name + '</option>').appendTo('select[name=searchJunle]');
				})
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				console.log('ジャンルエラー');
			}
		});
	});


});