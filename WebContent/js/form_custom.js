$(document).ready(function(){
	// form-----------------------------------
	$('input[type="text"], input[type="password"], select')
	.css('box-shadow', '0 0 1px 0 black')
	.focus(function() {
		$(this)
		.css('outline', '0')
		.css('box-shadow', '0 0 1px 0 rgba(255,153,0,1)');
	})
	.blur(function() {
		$(this).css('box-shadow', '0 0 1px 0 black');
	});
});