$(document).ready(function() {
	// カテゴリ選択時イベント
	$('select[name=searchCategory]').on('change', function(){
		setJunle();
	});
	// テーブル初期化処理
	$(function() {
		if ($('input[name=homeFeature]').val() != 0) {
			searchFeature();
		} else if ($('input[name=fromHome]').val()) {
//			alert("category: " + $('input[name=homeCategory]').val() + "¥n junle: " + $('input[name=homeJunle]').val() + "¥n name: " + $('input[name=homeName]').val());
			$('select[name=searchCategory]').val($('input[name=homeCategory]').val());
			setJunle();	// カテゴリセット時にジャンルを生成
			$('select[name=searchJunle]').val($('input[name=homeJunle]').val());
			$('input[name=searchName]').val($('input[name=homeName]').val());
			search();
		}
	});
	// 検索時イベント
	$('button#btn-search').click(function() {
		search();
	});

	//ページング機能のjQuery 全件とってきたレンタル一覧を10件ずつ表示する
	$('#prev').click(function() {
		if (page > 0) {
			page--;
			draw();
		}
	});
	$('#next').click(function() {
		if (page < ($('tr').size() - 1) / 10 - 1) {
			page++;
			draw();
		}
	});
});
var page = 0;

function draw() {
	$('#page').html(page + 1);
	$('tr').hide();
	$('tr:first,tr:gt(' + page * 10 + '):lt(10)').show();
};

//カテゴリの値によってジャンルを生成
function setJunle() {
	var category_val = $('select[name=searchCategory]').val();
	$('select[name=searchJunle] option').remove();
	$('<option value="" disabled selected>ジャンルを選択</option>').appendTo('select[name=searchJunle]');
	$.ajax({
		url: "getJunle",
		type: "POST",
		dataType: 'json',
		async: false,	// ホームから遷移時にこのメソッド呼び出し後にジャンルをセットする必要があるため同期処理
		data: {
			categoryId : category_val
		},
		success : function(data) {
			$('<option value="">すべて</option>').appendTo('select[name=searchJunle]');
			$(data.junleList).each(function() {
				$('<option value="' + this.id + '">' + this.name + '</option>').appendTo('select[name=searchJunle]');
			});
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log('ジャンルエラー');
		}
	});
};

//検索をかけて検索結果文字列、テーブル生成
function search() {
	var category_val = $('select[name=searchCategory]').val();
	var junle_val = $('select[name=searchJunle]').val();
	var name_val = $('input[name=searchName]').val();
	$.ajax({
		type: 'GET',
		url: 'utilSearchServlet',
		dataType: 'json',
		data: {
			categoryId : category_val,
			junleId : junle_val,
			name : name_val
		},
		success: function(data) {
			$('p#text-size').text('');
			$('p#text-size').append(data.listSize + '件見つかりました');
			$('p#text-result').text('');
			if (name_val || category_val || junle_val) {
				$('p#text-result').append('検索条件 ');
			}
			if (name_val) {
				$('p#text-result').append(' 検索語句:"' + data.searchName + '"');
			}
			if (category_val) {
				$('p#text-result').append(' カテゴリ:"' + data.searchCategory + '"');
			}
			if (junle_val) {
				$('p#text-result').append(' ジャンル:"' + data.searchJunle + '"');
			}

			$('table#table-result tr.tr-result').remove();
			$(data.merchandiseList).each(function() {
				$('<tr class="tr-result">' +
						'<td>' + this.line + '</td>' +
						'<td class="td-img"><img class="item-img" src="getImage?merchandise_id=' + this.id + '" /></td>' +
						'<td>' + this.name + '</td>' +
						'<td>' + this.arrival + '</td>' +
						'<td>' + this.category + '</td>' +
						'<td>' + this.junle + '</td>' +
						'<td>' + this.artist + '</td>' +
						'<td>' + this.price + '</td>' +
						'<td><button id="btn-cart" class="btn btn-default" type="button" onClick="insertCart(' + this.id + ')">カートへ</button></td>' +
						'</tr>').appendTo('table#table-result');
			})
			page = 0;
			// カート押下イベント
//			$('button#btn-cart').click(function() {
//				alert(this.val());
//				insertCart(this.val());
//			});
			draw();
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			 alert("リクエスト時になんらかのエラーが発生しました: " + textStatus + ":\n" + errorThrown);
		}
	});
};

//検索をかけて検索結果文字列、テーブル生成
function searchFeature() {
	var feature_val = $('input[name=homeFeature]').val();
	$.ajax({
		type: 'GET',
		url: 'getFeatureTarget',
		dataType: 'json',
		data: {
			featureId : feature_val
		},
		success: function(data) {
			$('p#text-size').text('');
			$('p#text-size').append(data.listSize + '件見つかりました');
			$('table#table-result tr.tr-result').remove();
			$(data.merchandiseList).each(function() {
				$('<tr class="tr-result">' +
						'<td>' + this.line + '</td>' +
						'<td><img class="item-img" src="getImage?merchandise_id=' + this.id + '" /></td>' +
						'<td>' + this.name + '</td>' +
						'<td>' + this.arrival + '</td>' +
						'<td>' + this.category + '</td>' +
						'<td>' + this.junle + '</td>' +
						'<td>' + this.artist + '</td>' +
						'<td>' + this.price + '</td>' +
						'<td><button class="btn btn-default" type="button" onClick="insertCart(' + this.id + ')">カートへ</button></td>' +
						'</tr>').appendTo('table#table-result');
			})
			page = 0;
			draw();
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			 alert("リクエスト時になんらかのエラーが発生しました: " + textStatus + ":\n" + errorThrown);
		}
	});
};

function insertCart(id) {
	alert('カートへ追加しました');
	$.ajax({
		type : 'POST',
		url : 'cart',
		data : {
			merchandiseId : id
		},
		success : function() {
			console.log("success");
		},
		error : function() {
			console.log("failed");
		}
	});
};
