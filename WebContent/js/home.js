$(document).ready(function() {
	// selectタグの自動生成
	$('select[name=searchCategory]').on('change', function() {
		var category_val = $('select[name=searchCategory]').val();
		$('select[name=searchJunle] option').remove();
		$('<option value="" disabled selected>ジャンルを選択</option>').appendTo('select[name=searchJunle]');
		$.ajax({
			url: "getJunle",
			type: "POST",
			dataType: 'json',
			data: {
				categoryId : category_val
			},
			success : function(data) {
				$('<option value="">すべて</option>').appendTo('select[name=searchJunle]');
				$(data.junleList).each(function() {
					$('<option value="' + this.id + '">' + this.name + '</option>').appendTo('select[name=searchJunle]');
				})
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				console.log('ジャンルエラー');
			}
		});
	});


	// 特集画像リサイズ(10:3)
//	$(window).on('load', function() { //画像サイズ取得するからonLoad
	$(function() {
		var img = $('.feature-img');
		var w = img.width();
		img.height(w * (3/10));
	});

	// キャプション------------------------------
//	$(window).on('load', function() { //画像サイズ取得するからonLoad
	$(function() {
		var div_h = $('.caption').height();
		var p_h = $('.caption').height() * 0.2;
		$('caption-p').height = p_h;
		$('.caption-p').css('top', div_h + p_h);
		$('.caption').hover(function () {
			$('.caption-p', this).animate({
				top: (div_h - p_h)
			}, 500);
		}, function () {
			$('.caption-p', this).animate({
				top: (div_h + p_h)
			}, 500);
		});
	});
	// slick-----------------------------------
	$('.multiple-items').slick({
		dots: true,
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 5
	});
});