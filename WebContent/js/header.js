$(document).ready(function() {
	// selectタグの自動生成
	$('select[name=searchCategory]').on('change', function() {
		var category_val = $(this).val();
		$.ajax({
			url: "getJunle",
			type: "POST",
			dataType: 'json',
			data: {
				categoryId : category_val
			},
			success : function(data) {
				$('select[name=searchJunle] option').remove();
				$('<option value="" disabled selected style="display:none;">ジャンルを選択</option>').appendTo('select[name=searchJunle]');
				$(data.junleList).each(function() {
					$('<option value="' + this.id + '">' + this.name + '</option>').appendTo('select[name=searchJunle]');
				})
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert("リクエスト時になんらかのエラーが発生しました: " + textStatus + ":\n" + errorThrown);
			}
		});
	});
});
